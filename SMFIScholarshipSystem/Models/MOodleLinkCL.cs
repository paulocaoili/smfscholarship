﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Text;
using System.Collections.Generic;

public class MoodleLinkCL
{
    public string wsLink = "http://10.57.30.132/moodle/webservice/rest/server.php?wstoken={0}&wsfunction={1}&moodlewsrestformat=json";
    public string token = "525abe9891db140937515dca73b22f13";
    public List<MoodleCreateUserResponse> newUsers;
    public MoodleException moodleError;
    public activityItems gradeItems;
    public string errorMessage = "";
    public int moodleid;
    public void createModelUser(MoodleUser userInfo)
    {
            MoodleUser user = new MoodleUser();
            user.username = HttpUtility.UrlEncode(userInfo.username);
            user.password = HttpUtility.UrlEncode(userInfo.password);
            user.firstname = HttpUtility.UrlEncode(userInfo.firstname);
            user.lastname = HttpUtility.UrlEncode(userInfo.lastname);
            user.email = HttpUtility.UrlEncode(userInfo.email  );

            String postData = String.Format("users[0][username]={0}&users[0][password]={1}&users[0][firstname]={2}&users[0][lastname]={3}&users[0][email]={4}", user.username, user.password, user.firstname, user.lastname, user.email);
            string createRequest = string.Format(this.wsLink, token, "core_user_create_users");


            List<MoodleUser> userList = new List<MoodleUser>();
            userList.Add(user);

            Array arrUsers = userList.ToArray();
            /*  
                String postData = String.Format("users[0][username]={0}&users[0][password]={1}&users[0][firstname]={2}&users[0][lastname]={3}&users[0][email]={4}", user.username, user.password, user.firstname, user.lastname, user.email);



                string createRequest = string.Format("http://10.57.30.132/moodle/webservice/rest/server.php?wstoken={0}&wsfunction={1}&moodlewsrestformat=json", token, "core_user_create_users");
            
                    String postData = String.Format("users[0][username]={0}&users[0][password]={1}&users[0][firstname]={2}&users[0][lastname]={3}&users[0][email]={4}", user.username, user.password, user.firstname, user.lastname, user.email);



                    string createRequest = string.Format("http://10.57.30.132/moodle/webservice/rest/server.php?wstoken={0}&wsfunction={1}&moodlewsrestformat=json", token, "core_user_create_users");
         
            

            String postData = String.Format("courseid={0}&userids[0]={1}", 2, 5);

            string createRequest = string.Format("http://10.57.30.132/moodle/webservice/rest/server.php?wstoken={0}&wsfunction={1}&moodlewsrestformat=json", token, "core_grades_get_grades");
         */

            // Call Moodle REST Service

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(createRequest);

            req.Method = "POST";

            req.ContentType = "application/x-www-form-urlencoded";

              // Encode the parameters as form data:

            byte[] formData =
                UTF8Encoding.UTF8.GetBytes(postData);
            req.ContentLength = formData.Length;

            // Write out the form Data to the request:
            using (Stream post = req.GetRequestStream())
            {
                post.Write(formData, 0, formData.Length);
            }


            // Get the Response
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            Stream resStream = resp.GetResponseStream();
            StreamReader reader = new StreamReader(resStream);
            string contents = reader.ReadToEnd();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if (contents.Contains("exception"))
            {
                // Error
                this.moodleError = serializer.Deserialize<MoodleException>(contents);
            }
            else
            {
                // Good
                //var newUsers =  serializer.Deserialize<object>(contents);
                List<MoodleCreateUserResponse> newUsers = serializer.Deserialize<List<MoodleCreateUserResponse>>(contents);
                MoodleEnrollment me = new MoodleEnrollment();
                me.userId = Convert.ToInt16(newUsers[0].id);
                this.moodleid = me.userId;
                me.courseId = 2;
                me.roleId = 5;
                this.enrollUser(me);
                
              
            }
        
            

    }

    public void enrollUser(MoodleEnrollment user) {

        long timeStart = DateTime.Now.Ticks;
        long timeEnd = DateTime.Now.AddMonths(1).Ticks;
        String postData = String.Format("enrolments[0][roleid]={0}&enrolments[0][userid]={1}&enrolments[0][courseid]={2}&enrolments[0][timestart]={3}&enrolments[0][timeend]={4}&enrolments[0][suspend]={5}", user.roleId, user.userId, user.courseId, timeStart, timeEnd, 0);
        string createRequest = string.Format(this.wsLink, token, "enrol_manual_enrol_users");


        
        // Call Moodle REST Service

        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(createRequest);

        req.Method = "POST";

        req.ContentType = "application/x-www-form-urlencoded";

        // Encode the parameters as form data:

        byte[] formData =
            UTF8Encoding.UTF8.GetBytes(postData);
        req.ContentLength = formData.Length;

        // Write out the form Data to the request:
        using (Stream post = req.GetRequestStream())
        {
            post.Write(formData, 0, formData.Length);
        }


        // Get the Response
        HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
        Stream resStream = resp.GetResponseStream();
        StreamReader reader = new StreamReader(resStream);
        string contents = reader.ReadToEnd();

        JavaScriptSerializer serializer = new JavaScriptSerializer();
        if (contents.Contains("exception"))
        {
            // Error
            MoodleException moodleError = serializer.Deserialize<MoodleException>(contents);
        }
        else
        {
            // Good
            var newUsers = serializer.Deserialize<object>(contents);
        }

        
    }


    public void getStudentExamResults() {
        
        try
        {
            this.errorMessage = "";
            String postData = String.Format("courseid={0}&userids[0]={1}", 2, this.moodleid);
            string createRequest = string.Format("http://10.57.30.132/moodle/webservice/rest/server.php?wstoken={0}&wsfunction={1}&moodlewsrestformat=json", token, "core_grades_get_grades");


            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(createRequest);

            req.Method = "POST";

            req.ContentType = "application/x-www-form-urlencoded";

            // Encode the parameters as form data:

            byte[] formData =
                UTF8Encoding.UTF8.GetBytes(postData);
            req.ContentLength = formData.Length;

            // Write out the form Data to the request:
            using (Stream post = req.GetRequestStream())
            {
                post.Write(formData, 0, formData.Length);
            }


            // Get the Response
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            Stream resStream = resp.GetResponseStream();
            StreamReader reader = new StreamReader(resStream);
            string contents = reader.ReadToEnd();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if (contents.Contains("exception"))
            {
                // Error
                this.moodleError = serializer.Deserialize<MoodleException>(contents);
            }
            else
            {
                // Good
                this.gradeItems = serializer.Deserialize<activityItems>(contents);
                // var gradeItems = serializer.Deserialize<object>(contents);

                //this.newUsers = serializer.Deserialize<List<MoodleCreateUserResponse>>(contents);
            }
        }
        catch (Exception ex) {
            this.errorMessage = ex.Message;
        }
    }



   
}
public class activityItems
{
    public List<activity> items { get; set; }
}
public class activity
{
    public string activityid { get; set; }
    public int itemnumber { get; set; }
    public int scaleid { get; set; }
    public string name { get; set; }
    public double grademin { get; set; }
    public double grademax { get; set; }
    public double gradepass { get; set; }
    public int locked { get; set; }
    public int hidden { get; set; }
    public userGrades[] grades { get; set; }
}


public class userGrades
{
    public int userid { get; set; }
    double grade { get; set; }
    public int locked { get; set; }
    public int hidden { get; set; }
    public int overridden { get; set; }
    public string feedback { get; set; }
    public int feedbackformat { get; set; }
    public int? usermodified { get; set; }
    public int? datesubmitted { get; set; }
    public int dategraded { get; set; }
    public string str_grade { get; set; }
    public string str_long_grade { get; set; }
    public string str_feedback { get; set; }
}
public class MoodleUser
{

    public string username { get; set; }

    public string password { get; set; }

    public string firstname { get; set; }

    public string lastname { get; set; }

    public string email { get; set; }

}


public class MoodleEnrollment
{

    public int roleId { get; set; }

    public int userId { get; set; }

    public int courseId { get; set; }

}

public class MoodleException
{

    public string exception { get; set; }

    public string errorcode { get; set; }

    public string message { get; set; }

    public string debuginfo { get; set; }

}


public class MoodleCreateUserResponse
{

    public string id { get; set; }

    public string username { get; set; }

}




