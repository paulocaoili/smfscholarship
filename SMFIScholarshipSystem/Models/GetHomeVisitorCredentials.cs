﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMFIScholarshipSystem.Models
{
    public class GetHomeVisitorCredentials
    {
        public int HomeVisitorUserId { get; set; }
        public string HomeVisitorName { get; set; }
        public string HomeVisitorEmail { get; set; }
        public string HomeVisitorPosition { get; set; }
    }
}