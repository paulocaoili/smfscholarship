﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMFIScholarshipSystem.Models
{
    public class GetAdminCredrentials
    {
        public int AdminUserId { get; set; }
        public string AdminFname { get; set; }
        public string AdminEmail { get; set; }
        public string Position { get; set; }
    }
}