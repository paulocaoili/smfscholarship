




-- -----------------------------------------------------------
-- Entity Designer DDL Script for MySQL Server 4.1 and higher
-- -----------------------------------------------------------
-- Date Created: 09/29/2016 13:35:33
-- Generated from EDMX file: C:\Users\nico.bulusan\Documents\Visual Studio 2013\Projects\SMFScholarshipSystem\SMFIERD\SMFIScholarshipSystem\Models\SMFIScholarshipModel.edmx
-- Target version: 3.0.0.0
-- --------------------------------------------------

DROP DATABASE IF EXISTS `smscholars`;
CREATE DATABASE `smscholars`;
USE `smscholars`;

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- NOTE: if the constraint does not exist, an ignorable error will be reported.
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------
SET foreign_key_checks = 0;
SET foreign_key_checks = 1;

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

CREATE TABLE `PersonalBackgrounds`(
	`SID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`ApplicationID` longtext NOT NULL, 
	`FirstName` longtext NOT NULL, 
	`LastName` longtext NOT NULL, 
	`MiddleName` longtext NOT NULL, 
	`ReligionID` int NOT NULL, 
	`Email` longtext NOT NULL, 
	`DateOfBirth` longtext NOT NULL, 
	`MaritalStatusID` int NOT NULL, 
	`Gender` longtext NOT NULL, 
	`PlaceOfBirth` longtext NOT NULL, 
	`Nationality` longtext NOT NULL, 
	`Height` longtext NOT NULL, 
	`Weight` longtext NOT NULL, 
	`AggregiateFamilyIncome` longtext NOT NULL, 
	`DatePosted` longtext NOT NULL);

ALTER TABLE `PersonalBackgrounds` ADD PRIMARY KEY (SID);




CREATE TABLE `ContactNumbers`(
	`CNID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`SID` int NOT NULL, 
	`Type` longtext NOT NULL, 
	`ContactNumber` longtext NOT NULL);

ALTER TABLE `ContactNumbers` ADD PRIMARY KEY (CNID);




CREATE TABLE `Religions`(
	`ReligionID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`Name` longtext NOT NULL);

ALTER TABLE `Religions` ADD PRIMARY KEY (ReligionID);




CREATE TABLE `MaritalStatus`(
	`MaritalStatusID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`Name` longtext NOT NULL);

ALTER TABLE `MaritalStatus` ADD PRIMARY KEY (MaritalStatusID);




CREATE TABLE `AcademicBackgrounds`(
	`ABID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`SID` int NOT NULL, 
	`HighSchoolName` longtext NOT NULL, 
	`Section` longtext NOT NULL, 
	`CompleteAddress` longtext NOT NULL, 
	`NameOfPrincipal` longtext NOT NULL);

ALTER TABLE `AcademicBackgrounds` ADD PRIMARY KEY (ABID);




CREATE TABLE `AcademicGrades`(
	`AGID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`Subject` longtext NOT NULL, 
	`Grade` longtext NOT NULL, 
	`ABID` int NOT NULL, 
	`SID` int NOT NULL);

ALTER TABLE `AcademicGrades` ADD PRIMARY KEY (AGID);




CREATE TABLE `AcademicOrganizationMemberships`(
	`AOMID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`Organization` longtext NOT NULL, 
	`PositionHeld` longtext NOT NULL, 
	`SID` int NOT NULL, 
	`ABID` int NOT NULL);

ALTER TABLE `AcademicOrganizationMemberships` ADD PRIMARY KEY (AOMID);




CREATE TABLE `AcademicAwards`(
	`AAID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`NameOfAward` longtext NOT NULL, 
	`AALID` int NOT NULL, 
	`SID` int NOT NULL, 
	`ABID` int NOT NULL);

ALTER TABLE `AcademicAwards` ADD PRIMARY KEY (AAID);




CREATE TABLE `AcademicAwardLevels`(
	`AALID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`Name` longtext NOT NULL);

ALTER TABLE `AcademicAwardLevels` ADD PRIMARY KEY (AALID);




CREATE TABLE `CollegePlans`(
	`CPID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`SID` int NOT NULL, 
	`SchoolCourseID` int NOT NULL);

ALTER TABLE `CollegePlans` ADD PRIMARY KEY (CPID);




CREATE TABLE `SchoolCourses`(
	`SchoolCourseID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`CourseID` int NOT NULL, 
	`CourseName` longtext NOT NULL, 
	`SchoolID` int NOT NULL);

ALTER TABLE `SchoolCourses` ADD PRIMARY KEY (SchoolCourseID);




CREATE TABLE `Schools`(
	`SchoolID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`SchoolName` longtext NOT NULL);

ALTER TABLE `Schools` ADD PRIMARY KEY (SchoolID);




CREATE TABLE `FamilyBackgrounds`(
	`FBID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`MemberName` longtext NOT NULL, 
	`FMID` int NOT NULL, 
	`DateOfBirth` longtext NOT NULL, 
	`EAID` int NOT NULL, 
	`LastSchoolAttended` longtext NOT NULL, 
	`NatureOfWork` longtext NOT NULL, 
	`Company` longtext NOT NULL, 
	`MaritalStatusID` int NOT NULL, 
	`SID` int NOT NULL);

ALTER TABLE `FamilyBackgrounds` ADD PRIMARY KEY (FBID);




CREATE TABLE `FamilyMembers`(
	`FMID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`Name` longtext NOT NULL);

ALTER TABLE `FamilyMembers` ADD PRIMARY KEY (FMID);




CREATE TABLE `EducationalAttainments`(
	`EAID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`Name` longtext NOT NULL);

ALTER TABLE `EducationalAttainments` ADD PRIMARY KEY (EAID);




CREATE TABLE `InterviewerComments`(
	`Id` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`InID` int NOT NULL, 
	`Comments` longtext NOT NULL, 
	`SID` int NOT NULL);

ALTER TABLE `InterviewerComments` ADD PRIMARY KEY (Id);




CREATE TABLE `Interviewers1`(
	`InID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`Name` longtext NOT NULL, 
	`Company` longtext NOT NULL, 
	`Designation` longtext NOT NULL);

ALTER TABLE `Interviewers1` ADD PRIMARY KEY (InID);




CREATE TABLE `ApplicationAttachments`(
	`AAID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`ATID` int NOT NULL, 
	`FIle` longtext NOT NULL, 
	`SID` int NOT NULL);

ALTER TABLE `ApplicationAttachments` ADD PRIMARY KEY (AAID);




CREATE TABLE `AttachmentTypes`(
	`ATID` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`Name` longtext NOT NULL);

ALTER TABLE `AttachmentTypes` ADD PRIMARY KEY (ATID);






-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on `ReligionID` in table 'PersonalBackgrounds'

ALTER TABLE `PersonalBackgrounds`
ADD CONSTRAINT `FK_PersonalBackgroundReligions`
    FOREIGN KEY (`ReligionID`)
    REFERENCES `Religions`
        (`ReligionID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonalBackgroundReligions'

CREATE INDEX `IX_FK_PersonalBackgroundReligions` 
    ON `PersonalBackgrounds`
    (`ReligionID`);

-- Creating foreign key on `SID` in table 'ContactNumbers'

ALTER TABLE `ContactNumbers`
ADD CONSTRAINT `FK_PersonalBackgroundContactNumbers`
    FOREIGN KEY (`SID`)
    REFERENCES `PersonalBackgrounds`
        (`SID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonalBackgroundContactNumbers'

CREATE INDEX `IX_FK_PersonalBackgroundContactNumbers` 
    ON `ContactNumbers`
    (`SID`);

-- Creating foreign key on `MaritalStatusID` in table 'PersonalBackgrounds'

ALTER TABLE `PersonalBackgrounds`
ADD CONSTRAINT `FK_PersonalBackgroundMaritalStatus`
    FOREIGN KEY (`MaritalStatusID`)
    REFERENCES `MaritalStatus`
        (`MaritalStatusID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonalBackgroundMaritalStatus'

CREATE INDEX `IX_FK_PersonalBackgroundMaritalStatus` 
    ON `PersonalBackgrounds`
    (`MaritalStatusID`);

-- Creating foreign key on `SID` in table 'AcademicBackgrounds'

ALTER TABLE `AcademicBackgrounds`
ADD CONSTRAINT `FK_PersonalBackgroundAcademicBackground`
    FOREIGN KEY (`SID`)
    REFERENCES `PersonalBackgrounds`
        (`SID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonalBackgroundAcademicBackground'

CREATE INDEX `IX_FK_PersonalBackgroundAcademicBackground` 
    ON `AcademicBackgrounds`
    (`SID`);

-- Creating foreign key on `ABID` in table 'AcademicGrades'

ALTER TABLE `AcademicGrades`
ADD CONSTRAINT `FK_AcademicBackgroundAcademicGrades`
    FOREIGN KEY (`ABID`)
    REFERENCES `AcademicBackgrounds`
        (`ABID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AcademicBackgroundAcademicGrades'

CREATE INDEX `IX_FK_AcademicBackgroundAcademicGrades` 
    ON `AcademicGrades`
    (`ABID`);

-- Creating foreign key on `AALID` in table 'AcademicAwards'

ALTER TABLE `AcademicAwards`
ADD CONSTRAINT `FK_AcademicAwardsAcademicAwardLevel`
    FOREIGN KEY (`AALID`)
    REFERENCES `AcademicAwardLevels`
        (`AALID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AcademicAwardsAcademicAwardLevel'

CREATE INDEX `IX_FK_AcademicAwardsAcademicAwardLevel` 
    ON `AcademicAwards`
    (`AALID`);

-- Creating foreign key on `ABID` in table 'AcademicOrganizationMemberships'

ALTER TABLE `AcademicOrganizationMemberships`
ADD CONSTRAINT `FK_AcademicBackgroundAcademicOrganizationMembership`
    FOREIGN KEY (`ABID`)
    REFERENCES `AcademicBackgrounds`
        (`ABID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AcademicBackgroundAcademicOrganizationMembership'

CREATE INDEX `IX_FK_AcademicBackgroundAcademicOrganizationMembership` 
    ON `AcademicOrganizationMemberships`
    (`ABID`);

-- Creating foreign key on `ABID` in table 'AcademicAwards'

ALTER TABLE `AcademicAwards`
ADD CONSTRAINT `FK_AcademicBackgroundAcademicAwards`
    FOREIGN KEY (`ABID`)
    REFERENCES `AcademicBackgrounds`
        (`ABID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AcademicBackgroundAcademicAwards'

CREATE INDEX `IX_FK_AcademicBackgroundAcademicAwards` 
    ON `AcademicAwards`
    (`ABID`);

-- Creating foreign key on `SchoolID` in table 'SchoolCourses'

ALTER TABLE `SchoolCourses`
ADD CONSTRAINT `FK_SchoolSchoolCourses`
    FOREIGN KEY (`SchoolID`)
    REFERENCES `Schools`
        (`SchoolID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SchoolSchoolCourses'

CREATE INDEX `IX_FK_SchoolSchoolCourses` 
    ON `SchoolCourses`
    (`SchoolID`);

-- Creating foreign key on `SchoolCourseID` in table 'CollegePlans'

ALTER TABLE `CollegePlans`
ADD CONSTRAINT `FK_SchoolCoursesCollegePlan`
    FOREIGN KEY (`SchoolCourseID`)
    REFERENCES `SchoolCourses`
        (`SchoolCourseID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SchoolCoursesCollegePlan'

CREATE INDEX `IX_FK_SchoolCoursesCollegePlan` 
    ON `CollegePlans`
    (`SchoolCourseID`);

-- Creating foreign key on `SID` in table 'FamilyBackgrounds'

ALTER TABLE `FamilyBackgrounds`
ADD CONSTRAINT `FK_PersonalBackgroundFamilyBackground`
    FOREIGN KEY (`SID`)
    REFERENCES `PersonalBackgrounds`
        (`SID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonalBackgroundFamilyBackground'

CREATE INDEX `IX_FK_PersonalBackgroundFamilyBackground` 
    ON `FamilyBackgrounds`
    (`SID`);

-- Creating foreign key on `FMID` in table 'FamilyBackgrounds'

ALTER TABLE `FamilyBackgrounds`
ADD CONSTRAINT `FK_FamilyMemberFamilyBackground`
    FOREIGN KEY (`FMID`)
    REFERENCES `FamilyMembers`
        (`FMID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_FamilyMemberFamilyBackground'

CREATE INDEX `IX_FK_FamilyMemberFamilyBackground` 
    ON `FamilyBackgrounds`
    (`FMID`);

-- Creating foreign key on `EAID` in table 'FamilyBackgrounds'

ALTER TABLE `FamilyBackgrounds`
ADD CONSTRAINT `FK_EducationalAttainmentFamilyBackground`
    FOREIGN KEY (`EAID`)
    REFERENCES `EducationalAttainments`
        (`EAID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EducationalAttainmentFamilyBackground'

CREATE INDEX `IX_FK_EducationalAttainmentFamilyBackground` 
    ON `FamilyBackgrounds`
    (`EAID`);

-- Creating foreign key on `MaritalStatusID` in table 'FamilyBackgrounds'

ALTER TABLE `FamilyBackgrounds`
ADD CONSTRAINT `FK_MaritalStatusFamilyBackground`
    FOREIGN KEY (`MaritalStatusID`)
    REFERENCES `MaritalStatus`
        (`MaritalStatusID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_MaritalStatusFamilyBackground'

CREATE INDEX `IX_FK_MaritalStatusFamilyBackground` 
    ON `FamilyBackgrounds`
    (`MaritalStatusID`);

-- Creating foreign key on `SID` in table 'InterviewerComments'

ALTER TABLE `InterviewerComments`
ADD CONSTRAINT `FK_PersonalBackgroundInterviewerComments`
    FOREIGN KEY (`SID`)
    REFERENCES `PersonalBackgrounds`
        (`SID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonalBackgroundInterviewerComments'

CREATE INDEX `IX_FK_PersonalBackgroundInterviewerComments` 
    ON `InterviewerComments`
    (`SID`);

-- Creating foreign key on `InID` in table 'InterviewerComments'

ALTER TABLE `InterviewerComments`
ADD CONSTRAINT `FK_InterviewersInterviewerComments`
    FOREIGN KEY (`InID`)
    REFERENCES `Interviewers1`
        (`InID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InterviewersInterviewerComments'

CREATE INDEX `IX_FK_InterviewersInterviewerComments` 
    ON `InterviewerComments`
    (`InID`);

-- Creating foreign key on `ATID` in table 'ApplicationAttachments'

ALTER TABLE `ApplicationAttachments`
ADD CONSTRAINT `FK_AttachmentTypeApplicationAttachments`
    FOREIGN KEY (`ATID`)
    REFERENCES `AttachmentTypes`
        (`ATID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AttachmentTypeApplicationAttachments'

CREATE INDEX `IX_FK_AttachmentTypeApplicationAttachments` 
    ON `ApplicationAttachments`
    (`ATID`);

-- Creating foreign key on `SID` in table 'ApplicationAttachments'

ALTER TABLE `ApplicationAttachments`
ADD CONSTRAINT `FK_PersonalBackgroundApplicationAttachments`
    FOREIGN KEY (`SID`)
    REFERENCES `PersonalBackgrounds`
        (`SID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonalBackgroundApplicationAttachments'

CREATE INDEX `IX_FK_PersonalBackgroundApplicationAttachments` 
    ON `ApplicationAttachments`
    (`SID`);

-- Creating foreign key on `SID` in table 'CollegePlans'

ALTER TABLE `CollegePlans`
ADD CONSTRAINT `FK_PersonalBackgroundCollegePlan`
    FOREIGN KEY (`SID`)
    REFERENCES `PersonalBackgrounds`
        (`SID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonalBackgroundCollegePlan'

CREATE INDEX `IX_FK_PersonalBackgroundCollegePlan` 
    ON `CollegePlans`
    (`SID`);

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
