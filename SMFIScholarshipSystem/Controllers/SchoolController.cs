﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class SchoolController : Controller
    {
        // GET: School
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddSchool(school skool)
        {
            try
            {
                smscholarsEntities smfi = new smscholarsEntities();
                school skul = new school();
                skul.SchoolID = skool.SchoolID;
                skul.SchoolName = skool.SchoolName;
                smfi.schools.Add(skul);
                smfi.SaveChanges();
                return Json(new { Result = "OK", Record = skul });
                
            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            //return Json
        }
        [HttpPost]
        public ActionResult UpdateSchool(school skool)
        {
            try
            {
                smscholarsEntities smfi = new smscholarsEntities();
                school skul = smfi.schools.Where(x => x.SchoolID == skool.SchoolID).FirstOrDefault();
                skul.SchoolID = skool.SchoolID;
                skul.SchoolName = skool.SchoolName;
                smfi.SaveChanges();
                SchoolModel scModel = new SchoolModel();
                scModel.SchoolID = skul.SchoolID;
                scModel.SchoolName = skul.SchoolName;
                return Json(new { Result = "OK", Record = scModel });


            }
            catch (Exception ex)
            {
                
                 return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteSchool(school skool)
        {
            try
            {
                smscholarsEntities smfi = new smscholarsEntities();
                school skul = smfi.schools.Where(x => x.SchoolID == skool.SchoolID).FirstOrDefault();
                if (skul == null)
                {
                    throw new Exception("School Type not found!");
                }
                smfi.schools.Remove(skul);
                smfi.SaveChanges();

                return Json(new { Result = "OK" });

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}