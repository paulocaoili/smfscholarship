﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;
using SMFIScholarshipSystem.Models;

namespace SMFIScholarshipSystem.Controllers
{
    public class InterviewerController : Controller
    {
        // GET: Interviewer
 
        public ActionResult Index()
        {
            return View();
        }
       
        // Insert Interviewer
        [HttpPost]
        public ActionResult AddInterviewer(smuser i)
        {
            try
            {
                smscholarsEntities smfi = new smscholarsEntities();
                smuser newInterviewer = new smuser();

                var encryptPass = CustomEnrypt.Encrypt(i.Password);
                newInterviewer.InID = i.InID;
                newInterviewer.Name = i.Name;
                newInterviewer.Company = i.Company;
                newInterviewer.Designation = i.Designation;
                newInterviewer.Username = i.Username;
                newInterviewer.Email = i.Email;
                newInterviewer.Password = encryptPass;
                newInterviewer.Position = i.Position;
                newInterviewer.ContactNumber = i.ContactNumber;
                smfi.smusers.Add(newInterviewer);
                smfi.SaveChanges();              
                return Json(new { Result = "OK", Record = newInterviewer });
            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult UpdateInterviewer(smuser i)
        {
            try
            {
                smscholarsEntities smfi = new smscholarsEntities();
                smuser newInter = smfi.smusers.Where(x => x.InID == i.InID).FirstOrDefault();
                if (newInter == null)
                {
                    throw new Exception("Interviewer not found!");
                }

                newInter.InID = i.InID;
                newInter.Name = i.Name;
                newInter.Company = i.Company;
                newInter.Designation = i.Designation;
                newInter.ContactNumber = i.ContactNumber;

                smfi.SaveChanges();

                InterviewModel im = new InterviewModel();
                im.InID = newInter.InID;
                im.Name = newInter.Name;
                im.Company = newInter.Company;
                im.Designation = newInter.Designation;
                im.ContactNumber = newInter.ContactNumber;
                return Json(new { Result = "OK", Record = im });

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }        
        }
        [HttpPost]
        public ActionResult DeleteInterviewer(smuser i)
        {
            try
            {
                smscholarsEntities smfi = new smscholarsEntities();
                smuser newInter = smfi.smusers.Where(x => x.InID == i.InID).FirstOrDefault();
                if (newInter == null)
                {
                    throw new Exception("Interviewer not found!");
                }
                smfi.smusers.Remove(newInter);
                smfi.SaveChanges();

                return Json(new { Result = "OK"});

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
       
        
    }
}