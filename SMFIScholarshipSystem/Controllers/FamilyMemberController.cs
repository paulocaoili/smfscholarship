﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class FamilyMemberController : Controller
    {
        // GET: FamilyMember
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddFamilyMember(familymember famMember)
        {
            try
            {
                smscholarsEntities smfi = new smscholarsEntities();
                familymember NewFam = new familymember();
                NewFam.FMID = famMember.FMID;
                NewFam.Name = famMember.Name;

                smfi.familymembers.Add(NewFam);
                smfi.SaveChanges();
                return Json(new { Result = "OK", Record = NewFam });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            //return Json
        }
        [HttpPost]
        public ActionResult UpdateFamilyMember(familymember famMember)
        {
            try
            {
                smscholarsEntities smfi = new smscholarsEntities();
                familymember NewFam = smfi.familymembers.Where(x => x.FMID == famMember.FMID).FirstOrDefault();
                NewFam.FMID = famMember.FMID;
                NewFam.Name = famMember.Name;
                smfi.SaveChanges();
                FamilyMemberModel FmModel = new FamilyMemberModel();
                FmModel.FMID = NewFam.FMID;
                FmModel.Name = NewFam.Name;
                return Json(new { Result = "OK", Record = FmModel });


            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteFamilyMember(familymember famMember)
        {
            try
            {
                smscholarsEntities smfi = new smscholarsEntities();
                familymember NewFam = smfi.familymembers.Where(x => x.FMID == famMember.FMID).FirstOrDefault();
                if (NewFam == null)
                {
                    throw new Exception("Family member not found!");
                }
                smfi.familymembers.Remove(NewFam);
                smfi.SaveChanges();

                return Json(new { Result = "OK" });

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}