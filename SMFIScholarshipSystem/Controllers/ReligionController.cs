﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class ReligionController : Controller
    {
        smscholarsEntities sm = new smscholarsEntities();    
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddReligion(religion rg)
        {
            try
            {
                religion newRg = new religion();
                newRg.Name = rg.Name;
                    sm.religions.Add(newRg);
                sm.SaveChanges();
                return Json(new { Result = "OK", Record = newRg });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateReligion(religion rg)
        {
            try
            {
                religion newRg = sm.religions.Where(x => x.ReligionID == rg.ReligionID).FirstOrDefault();
                newRg.ReligionID = rg.ReligionID;
                newRg.Name = rg.Name;
                sm.SaveChanges();
                ReligionModel newRgModel = new ReligionModel();
                newRgModel.Name = newRg.Name;
                newRgModel.ReligionID = newRg.ReligionID;
                return Json(new { Result = "OK", Record = newRgModel });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteReligion(religion rg)
        {
            try
            {
                religion newRg = sm.religions.Where(x => x.ReligionID == rg.ReligionID).FirstOrDefault();
                if (newRg == null)
                {
                    throw new Exception("No Religion Found");
                }
                sm.religions.Remove(newRg);
                sm.SaveChanges();

                return Json(new { Result = "OK" });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}