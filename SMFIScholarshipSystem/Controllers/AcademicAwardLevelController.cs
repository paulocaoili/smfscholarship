﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class AcademicAwardLevelController : Controller
    {
        // GET: AcademicAwardLevel
         [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult InsertAcadAwardlevel(academicawardlevel acadLevl)
        {
            try
            {
                      
                using (smscholarsEntities smfi = new smscholarsEntities())
                {
                    academicawardlevel NewAcdLvl = new academicawardlevel();   
                    NewAcdLvl.AALID = acadLevl.AALID;
                    NewAcdLvl.Name = acadLevl.Name;
                    smfi.academicawardlevels.Add(NewAcdLvl);
                    smfi.SaveChanges();
                    return Json(new { Result = "OK", Record = NewAcdLvl });
                }
              
            }
            catch (Exception ex)
            {
                
               
                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
          
        }
        [HttpPost]
        public ActionResult UpdateAcadAwardlevel(academicawardlevel acadLevl)
        {
            try
            {
                
                using (smscholarsEntities smfi = new smscholarsEntities())
                {
                    academicawardlevel ai = smfi.academicawardlevels.Where(x => x.AALID == acadLevl.AALID).FirstOrDefault();
                    if (ai == null)
                    {
                         throw new Exception("Academic Level not found!");
                         
                    }
                    ai.AALID = acadLevl.AALID;
                    ai.Name = acadLevl.Name;
                    smfi.SaveChanges();
                    AcademicAwardLevelModel acadlvlModel = new AcademicAwardLevelModel();

                    acadlvlModel.AALID = ai.AALID;
                    acadlvlModel.Name = ai.Name;
                    return Json(new { Result = "OK", Record = acadlvlModel });

                }
               
            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteAcadAwardlevel(academicawardlevel acadLevl)
        {
            try
            {
                using (smscholarsEntities smfi = new smscholarsEntities())
                {
                    academicawardlevel ai = smfi.academicawardlevels.Where(x => x.AALID == acadLevl.AALID).FirstOrDefault();
                    if (ai == null)
                    {
                        throw new Exception("Academic Level not found!");
                    }
                    smfi.academicawardlevels.Remove(ai);
                    smfi.SaveChanges();
                }
                 return Json(new { Result = "OK"});
            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}