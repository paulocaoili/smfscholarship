﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;


namespace SMFIScholarshipSystem.Controllers
{
    public class ShowMaintenanceController : Controller
    {
        // GET: ShowMaintenance
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetInterviewer()
        {

            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from inter in smfi.smusers
                          select new InterviewModel
                          {
                              InID = inter.InID,
                              Name = inter.Name,
                              Company = inter.Company,
                              Designation = inter.Designation,
                              ContactNumber = inter.ContactNumber,

                          }).ToList<InterviewModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetInterviewerOnly()
        {

            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from inter in smfi.smusers
                          where inter.Position == "Interviewer"
                          select new InterviewModel
                          {
                              InID = inter.InID,
                              Name = inter.Name,
                              Company = inter.Company,
                              Designation = inter.Designation

                          }).ToList<InterviewModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetHomeVisitorOnly()
        {

            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from inter in smfi.smusers
                          where inter.Position == "Home Visitor"
                          select new InterviewModel
                          {
                              InID = inter.InID,
                              Name = inter.Name,
                              Company = inter.Company,
                              Designation = inter.Designation

                          }).ToList<InterviewModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetSchool()
        {
            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from skul in smfi.schools
                          select new SchoolModel
                          {
                              SchoolID = skul.SchoolID,
                              SchoolName = skul.SchoolName
                          }).ToList<SchoolModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetFamilyMembers()
        {
            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from Fam in smfi.familymembers
                          select new FamilyMemberModel
                          {
                              FMID = Fam.FMID,
                              Name = Fam.Name
                          }).ToList<FamilyMemberModel>();
            return Json(result , JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMaritalStatus()
        {
            smscholarsEntities smfi = new smscholarsEntities();

             var result = (from CivilStat in smfi.maritalstatus
                          select new MaritalStatusModel
                          {
                            MaritalStatusID = CivilStat.MaritalStatusID,
                            Name = CivilStat.Name
                          }).ToList<MaritalStatusModel>();
            return Json(result , JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Getreligion()
        {

             smscholarsEntities smfi = new smscholarsEntities();
              var result = (from Relihiyon in smfi.religions
                          select new ReligionModel
                          {
                            ReligionID = Relihiyon.ReligionID,
                            Name = Relihiyon.Name
                          }).ToList<ReligionModel>();
             return Json(result , JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetEducationalAttainment()
        {
            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from EducAttain in smfi.educationalattainments
                          select new EducationalAttainmentModel
                          {
                              EAID = EducAttain.EAID,
                              Name = EducAttain.Name
                          }).ToList<EducationalAttainmentModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAcademicAwardLevel()
        {
            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from AwardLvl in smfi.academicawardlevels
                          select new AcademicAwardLevelModel
                          {
                              AALID = AwardLvl.AALID,
                              Name = AwardLvl.Name
                          }).ToList<AcademicAwardLevelModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetQuestionnaire()
        {

            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from Questionnaire in smfi.questionnaires
                          select new QuestionnaireModel
                          {
                              QuestionId = Questionnaire.QuestionId,
                              Question = Questionnaire.Question,
                              Year = Questionnaire.Year,
                              CategoryId = Questionnaire.CategoryId

                          }).ToList<QuestionnaireModel>();   
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetQuestionnaireCategory()
        {

            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from QuestionnaireCategory in smfi.questionnairecategories
                          select new QuestionnaireCategoryModel
                          {
                              CategoryId = QuestionnaireCategory.CategoryId,
                              Description = QuestionnaireCategory.Description

                          }).ToList<QuestionnaireCategoryModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetInterviewHeader(personalbackground bg)
        {
            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from InterviewHeader in smfi.homevisitheaders
                          where InterviewHeader.SID == bg.SID
                          select new InterviewHeaderModel
                          {
                              InterviewId = InterviewHeader.InterviewId,
                              SID = InterviewHeader.SID,
                              DatePosted = InterviewHeader.DatePosted,
                              Status = InterviewHeader.Status,
                              ApprovedBy = InterviewHeader.ApprovedBy,

                          }).ToList<InterviewHeaderModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetGWAs()
        {
            smscholarsEntities db = new smscholarsEntities();

            var result = (from g in db.gwas
                          select new gwaModel
                          {
                              ID = g.ID,
                              GWA1 = g.GWA1
                          }).ToList<gwaModel>();

            return Json(result, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult GetFamilyIncomes()
        {
            smscholarsEntities db = new smscholarsEntities();

            var result = (from f in db.familyincomes
                          select new FamilyIncomeModel
                          {
                              ID = f.ID,
                              Income = f.Income
                          }).ToList<FamilyIncomeModel>();

            return Json(result, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult GetGWAforStudents()
        {
            smscholarsEntities db = new smscholarsEntities();

            var result = (from gs in db.gwaforstudents
                          select new GWAforStudentsModel
                          {
                              SID = gs.SID,
                              ID = gs.ID,
                              GWA = gs.GWA,
                              ABID = gs.ABID,

                          }).ToList<GWAforStudentsModel>();

            return Json(result, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult GetStudentForInterview()
        {
            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from studs in smfi.personalbackgrounds
                          select new PersonalBgAdminModel
                          {
                              SID = studs.SID,
                              FirstName = studs.FirstName,
                              LastName = studs.LastName,
                              MiddleName = studs.MiddleName,
                            
                              Status = studs.Status,



                          }).ToList<PersonalBgAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetStudentInterviewResult(PersonalBgAdminModel pid)
        {
            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from studs in smfi.studentinterviews
                          where studs.SID == pid.SID
                          select new InterviewResult
                          {
                              SID = studs.SID,
                              InID = studs.InID,
                              Questions = studs.Questions,
                              Grade = studs.Grade,
                              Remarks = studs.Remarks,

                          }).ToList<InterviewResult>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetInterviewerName(PersonalBgAdminModel pid)
        {
            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from studs in smfi.studentinterviews
                          where studs.SID == pid.SID
                          select new InterviewResult
                          {
                              
                              InID = studs.InID,


                          }).Distinct().ToList<InterviewResult>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetQualifiedAreaname()
        { 
             smscholarsEntities db = new smscholarsEntities();

            var result = (from nameArea in db.qualifiedareas
                          select new QualifiedAreaAdminView
                          {
                              Name = nameArea.Name

                          }).Distinct().ToList<QualifiedAreaAdminView>();

            return Json(result, JsonRequestBehavior.AllowGet);
             
        }
    
    }
}