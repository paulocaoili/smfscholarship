﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class GWAController : Controller
    {
        // GET: GWA
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(gwa g)
        {
            try
            {
                smscholarsEntities db = new smscholarsEntities();

                gwa newGWAs = new gwa();

                newGWAs.ID = g.ID;
                newGWAs.GWA1 = g.GWA1;
                db.gwas.Add(newGWAs);
                db.SaveChanges();


                return Json(new { Result = "OK", Record = newGWAs });


            }
            catch (Exception ex)
            {
                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Update(gwa g)
        {
            try
            {
                smscholarsEntities db = new smscholarsEntities();
                gwa newGWAs = db.gwas.Where(x => x.ID == g.ID).FirstOrDefault();
                if (newGWAs == null)
                {
                    throw new Exception("CantUpdate");
                }
                newGWAs.ID = g.ID;
                newGWAs.GWA1 = g.GWA1;

                db.SaveChanges();
                gwaModel gm = new gwaModel();
                gm.ID = newGWAs.ID;
                gm.GWA1 = newGWAs.GWA1;

                return Json(new { Result = "OK", Record = gm });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Delete(gwa g)
        {
            try
            {
                smscholarsEntities db = new smscholarsEntities();
                gwa newGWAs = db.gwas.Where(x => x.ID == g.ID).FirstOrDefault();
                if (newGWAs == null)
                {
                    throw new Exception("GWA Not Found!");
                }

                db.gwas.Remove(newGWAs);

                db.SaveChanges();


                return Json(new { Result = "OK" });


            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}