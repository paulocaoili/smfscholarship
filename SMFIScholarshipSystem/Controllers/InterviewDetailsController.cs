﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class InterviewDetailsController : Controller
    {
        // GET: InterviewDetails
        smscholarsEntities db = new smscholarsEntities();
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Index()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetHomeVisitorCredentials)Session["HomeVisitorCredentials"];
            var myAdminCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null && myAdminCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else if (myCredential != null)
            {
                if (myCredential.HomeVisitorPosition == "Home Visitor")
                {
                    return View();
                }
                else if (myCredential.HomeVisitorPosition == "Approver")
                {
                    return RedirectToAction("Index", "ApproveStatusForStudent");
                }
                else if (myCredential.HomeVisitorPosition == "Interviewer")
                {
                    return RedirectToAction("Index", "InterviewStudents");
                }     
            }
            else if (myAdminCredential != null)
            {
                if (myAdminCredential.Position == "Admin")
                {
                    return RedirectToAction("Index", "ShowAllStudentAdmin" , new { id = myAdminCredential.AdminUserId });
                   
                }
                else
                {
                    return RedirectToAction("Authenticate", "UserAuthentication");
                }
            }
            return RedirectToAction("Authenticate", "UserAuthentication");
            
           
        }
        
    }
}