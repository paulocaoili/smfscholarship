﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class QuestionnaireController : Controller
    {
        smscholarsEntities db = new smscholarsEntities();    

        // GET: Questionnaire
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddQuestionnaire(questionnaire q)
        {
            try
            {
                questionnaire newQuestionnaire = new questionnaire();

                newQuestionnaire.QuestionId = q.QuestionId;
                newQuestionnaire.Question = q.Question;
                newQuestionnaire.Year = q.Year;
                newQuestionnaire.CategoryId = q.CategoryId;

                db.questionnaires.Add(newQuestionnaire);
                db.SaveChanges();
                return Json(new { Result = "OK", Record = newQuestionnaire });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateQuestionnaire(questionnaire q)
        {
            try
            {
                questionnaire newQuestionnaire = db.questionnaires.Where(x => x.QuestionId == q.QuestionId).FirstOrDefault();
                newQuestionnaire.QuestionId = q.QuestionId;
                newQuestionnaire.Question = q.Question;
                newQuestionnaire.Year = q.Year;
                newQuestionnaire.CategoryId = q.CategoryId;

                db.SaveChanges();
                QuestionnaireModel newQuestionnaireModel = new QuestionnaireModel();

                newQuestionnaireModel.QuestionId = newQuestionnaire.QuestionId;
                newQuestionnaireModel.Question = newQuestionnaire.Question;
                newQuestionnaireModel.Year = newQuestionnaire.Year;
                newQuestionnaireModel.CategoryId = newQuestionnaire.CategoryId;

                return Json(new { Result = "OK", Record = newQuestionnaireModel });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteQuestionnaire(questionnaire q)
        {
            try
            {
                questionnaire newQuestionnaire = db.questionnaires.Where(x => x.QuestionId == q.QuestionId).FirstOrDefault();
                if (newQuestionnaire == null)
                {
                    throw new Exception("No Questionnaire Found");
                }
                db.questionnaires.Remove(newQuestionnaire);
                db.SaveChanges();

                return Json(new { Result = "OK" });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}