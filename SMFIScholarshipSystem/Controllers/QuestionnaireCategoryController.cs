﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class QuestionnaireCategoryController : Controller
    {
        // GET: QuestionnaireCategory
        smscholarsEntities db = new smscholarsEntities();    

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddQuestionnaireCategory(questionnairecategory qc)
        {
            try
            {
                questionnairecategory newQuestionnaireCategory = new questionnairecategory();

                newQuestionnaireCategory.CategoryId = qc.CategoryId;
                newQuestionnaireCategory.Description = qc.Description;

                db.questionnairecategories.Add(newQuestionnaireCategory);
                db.SaveChanges();
                return Json(new { Result = "OK", Record = newQuestionnaireCategory });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateQuestionnaireCategory(questionnairecategory qc)
        {
            try
            {
                questionnairecategory newQuestionnaireCategory = db.questionnairecategories.Where(x => x.CategoryId == qc.CategoryId).FirstOrDefault();
                newQuestionnaireCategory.CategoryId = qc.CategoryId;
                newQuestionnaireCategory.Description = qc.Description;

                db.SaveChanges();
                QuestionnaireCategoryModel newQuestionnaireCategoryModel = new QuestionnaireCategoryModel();

                newQuestionnaireCategoryModel.CategoryId = newQuestionnaireCategory.CategoryId;
                newQuestionnaireCategoryModel.Description = newQuestionnaireCategory.Description;

                return Json(new { Result = "OK", Record = newQuestionnaireCategoryModel });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteQuestionnaireCategory(questionnairecategory qc)
        {
            try
            {
                questionnairecategory newQuestionnaireCategory = db.questionnairecategories.Where(x => x.CategoryId == qc.CategoryId).FirstOrDefault();
                if (newQuestionnaireCategory == null)
                {
                    throw new Exception("No Questionnaire Found");
                }
                db.questionnairecategories.Remove(newQuestionnaireCategory);
                db.SaveChanges();

                return Json(new { Result = "OK" });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}