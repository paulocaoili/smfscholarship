﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;
namespace SMFIScholarshipSystem.Controllers
{
    public class ApproveStatusForStudentController : Controller
    {
        // GET: ApproveStatusForStudent
        public ActionResult Index()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetHomeVisitorCredentials)Session["InterviewerCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Loginuser", "UserAuthentication");
            }
            else
            {
                return View();
            }
           
        }
        public ActionResult createHeader(homevisitdetail Idlo, homevisitheader header, List<homevisitdetail> updateList)
        {
            try
            {
                string updateStatusStudent;
                var myCredential = (SMFIScholarshipSystem.Models.GetHomeVisitorCredentials)Session["InterviewerCredentials"];
                smscholarsEntities db = new smscholarsEntities();
                homevisitheader newHeader = new homevisitheader();
                newHeader.SID = Idlo.SID;
                newHeader.DatePosted = DateTime.Now.ToString("MM-dd-yyyy");
                newHeader.Status = header.Status;
                newHeader.ApprovedBy = Convert.ToInt16(myCredential.HomeVisitorUserId);
                db.homevisitheaders.Add(newHeader);
                db.SaveChanges();
                if (newHeader.Status == "Accepted")
                {
                    updateStatusStudent = "Done For Approval-Accepted";
                }
                else
                {
                    updateStatusStudent = "Done For Approval-Disqualified";
                }
                personalbackground UpdatePersonalBgStatus = db.personalbackgrounds.Where(x => x.SID == Idlo.SID).FirstOrDefault();
                UpdatePersonalBgStatus.Status = updateStatusStudent;
                db.SaveChanges();
             
                foreach (var item in updateList)
                {
                    var newDetail = db.homevisitdetails.Where(x => x.SID == item.SID && x.InterviewEntryId == item.InterviewEntryId).FirstOrDefault();
                    newDetail.InterviewId = newHeader.InterviewId;
                    
                }
                db.SaveChanges();
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                
                 return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        
    }
}