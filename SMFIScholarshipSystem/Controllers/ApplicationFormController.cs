﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;
using System.IO;
using SMFIScholarshipSystem.MailerService;
using SMFIScholarshipSystem.Models;


namespace SMFIScholarshipSystem.Controllers
{
    public class ApplicationFormController : Controller
    {
        
        public int sid;
        smscholarsEntities sm = new smscholarsEntities();
        // GET: ApplicationForm
        public ActionResult Index(ApplicationFormModel pd)
        {
            return View(pd);
        }
        public ActionResult RegisterAdmin()
        {
            return View();
        }
        //public ActionResult sample(ApplicationFormModel pd)
        //{
        //    return View(pd);
        //}
        [HttpPost]
        public ActionResult AddStudAppForm(ApplicationFormModel personalBg)
        {

            Guid g = Guid.NewGuid();
            personalbackground NewPersonalbg = new personalbackground();
            academicbackground NewAcadBg = new academicbackground();
            gwaforstudent newGwas = new gwaforstudent();
            try
            {
                DateTime studdob = Convert.ToDateTime(personalBg.personalBgs.DateOfBirth);//;;
                
                var encryptPass = CustomEnrypt.Encrypt(personalBg.personalBgs.Password);

                religion newrg = new religion();
                if (personalBg.personalBgs.ReligionID == 34)
                {
                    newrg.Name = personalBg.personalBgs.OtherReligionID;
                    sm.religions.Add(newrg);
                    sm.SaveChanges();
                    NewPersonalbg.ReligionID = newrg.ReligionID;
                }
                else 
                {
                    NewPersonalbg.ReligionID = personalBg.personalBgs.ReligionID;
                }
               
              
                Session["SidKo"] = null;
                NewPersonalbg.ApplicationID = g.ToString();
                NewPersonalbg.FirstName = personalBg.personalBgs.FirstName;
                NewPersonalbg.LastName = personalBg.personalBgs.LastName;
                NewPersonalbg.MiddleName = personalBg.personalBgs.MiddleName;

               // NewPersonalbg.ReligionID = personalBg.personalBgs.ReligionID;
                NewPersonalbg.Email = personalBg.personalBgs.Email;
                NewPersonalbg.DateOfBirth = studdob.ToString("MM-dd-yyyy");
                NewPersonalbg.MaritalStatusID = personalBg.personalBgs.MaritalStatusID;
                NewPersonalbg.Gender = personalBg.personalBgs.Gender;
                NewPersonalbg.PlaceOfBirth = personalBg.personalBgs.PlaceOfBirth;
                NewPersonalbg.Nationality = personalBg.personalBgs.Nationality;
                NewPersonalbg.Height = personalBg.personalBgs.Height;
                NewPersonalbg.PermanentAddress = personalBg.personalBgs.PermanentAddress;
                NewPersonalbg.Weight = personalBg.personalBgs.Weight;
                NewPersonalbg.AggregiateFamilyIncome = personalBg.personalBgs.AggregiateFamilyIncome;
                NewPersonalbg.Password = encryptPass;
                NewPersonalbg.IsActive = 0;
                NewPersonalbg.ExamSched = false;
               
                NewPersonalbg.DatePosted = DateTime.Now.ToString("MM-dd-yyyy");
                NewPersonalbg.Status = "For Confirmation";
                NewPersonalbg.HomeAddress = personalBg.personalBgs.HomeAddress;
                sm.personalbackgrounds.Add(NewPersonalbg);
                sm.SaveChanges();
                Session["SidKo"] = NewPersonalbg.SID;
                sid = NewPersonalbg.SID;
                foreach (var item in personalBg.contactInfo)
                {
                    contactnumber cn = new contactnumber();
                    
                    cn.SID = NewPersonalbg.SID;

                    cn.Type = item.Type;
                    cn.ContactNumber1 = item.ContactNumber1;
                    sm.contactnumbers.Add(cn);

                }
                sm.SaveChanges();
                NewAcadBg.SID = NewPersonalbg.SID;
                NewAcadBg.HighSchoolName = personalBg.Academicbackground.HighSchoolName;
                NewAcadBg.Section = personalBg.Academicbackground.Section;
                NewAcadBg.CompleteAddress = personalBg.Academicbackground.CompleteAddress;
                NewAcadBg.NameOfPrincipal = personalBg.Academicbackground.NameOfPrincipal;
                NewAcadBg.SchoolTelno = personalBg.Academicbackground.SchoolTelno;
                NewAcadBg.MunicipalityOfSchoolAddress = personalBg.municitySchool;
                sm.academicbackgrounds.Add(NewAcadBg);
                sm.SaveChanges();
                newGwas.ABID = NewAcadBg.ABID;
                newGwas.GWA = personalBg.gwaofStud.GWA;
                newGwas.SID = NewPersonalbg.SID;
                sm.gwaforstudents.Add(newGwas);
                sm.SaveChanges();
                foreach (var itemGr in personalBg.AcademicGrade)
                {
                    academicgrade acgrade = new academicgrade();
                    acgrade.SID = NewPersonalbg.SID;
                    acgrade.ABID = NewAcadBg.ABID;
                    acgrade.Subject = itemGr.Subject;
                    acgrade.Grade = itemGr.Grade;
                    sm.academicgrades.Add(acgrade);
                }
                sm.SaveChanges();
                foreach (var NewacadorgMem in personalBg.acadorg)
                {
                    academicorganizationmembership acorg = new academicorganizationmembership();

                    if ((NewacadorgMem.Organization != null) && (NewacadorgMem.PositionHeld != null))
                    {
                        acorg.SID = NewPersonalbg.SID;
                        acorg.ABID = NewAcadBg.ABID;
                        acorg.Organization = NewacadorgMem.Organization;
                        acorg.PositionHeld = NewacadorgMem.PositionHeld;
                        sm.academicorganizationmemberships.Add(acorg);
                    }
                   
                }
                sm.SaveChanges();
                foreach (var NewAcadAwardee in personalBg.acadAwards)
                {
                    academicaward acadaward = new academicaward();
                    if ((NewAcadAwardee.AALID != 0) && (NewAcadAwardee.NameOfAward != null)&&(NewPersonalbg.SID != 0)&&(NewAcadBg.ABID != 0 ))
                    {
                        acadaward.NameOfAward = NewAcadAwardee.NameOfAward;
                        acadaward.SID = NewPersonalbg.SID;
                        acadaward.ABID = NewAcadBg.ABID;
                        acadaward.AALID = NewAcadAwardee.AALID;
                        sm.academicawards.Add(acadaward);
                    }
                   
                }
                sm.SaveChanges();
                foreach (var Newcolplan in personalBg.colpnas)
                {
                    collegeplan colplan = new collegeplan();
                    if ((NewPersonalbg.SID != 0) && (Newcolplan.SchoolID != 0) && (Newcolplan.SchoolCourseID != 0))
                    {
                        colplan.SID = NewPersonalbg.SID;
                        colplan.SchoolID = Newcolplan.SchoolID;
                        colplan.SchoolCourseID = Newcolplan.SchoolCourseID;
                        sm.collegeplans.Add(colplan);
                    }
                  

                }
                sm.SaveChanges();
                foreach (var newAcadTrack in personalBg.acadtrack)
                {
                    academictrack newAtrack = new academictrack();
                    newAtrack.SID = NewPersonalbg.SID;
                    newAtrack.ABID = NewAcadBg.ABID;
                    newAtrack.GradeNumber = newAcadTrack.GradeNumber;
                    newAtrack.AcademicTrack1 = newAcadTrack.AcademicTrack1;
                    sm.academictracks.Add(newAtrack);
                }
                sm.SaveChanges();
                foreach (var NewFamBack in personalBg.famBack)
                {
                    DateTime dob = Convert.ToDateTime(NewFamBack.DateOfBirth);//;
                    familybackground fg = new familybackground();
                    if ((NewFamBack.MemberName !=null) && 
                        (NewFamBack.DateOfBirth != null)&&
                        (NewFamBack.FMID != 0)&&
                        (NewFamBack.EAID != 0)&&
                        (NewFamBack.LastSchoolAttended != null)&&
                        (NewFamBack.NatureOfWork != null)&&
                        (NewFamBack.Company != null)&&
                        (NewFamBack.MaritalStatusID != 0)&&
                        (NewPersonalbg.SID != 0))
                    {
                        fg.MemberName = NewFamBack.MemberName;
                        fg.FMID = NewFamBack.FMID;
                        fg.DateOfBirth = dob.ToString("MM-dd-yyyy");
                        fg.EAID = NewFamBack.EAID;
                        fg.LastSchoolAttended = NewFamBack.LastSchoolAttended;
                        fg.NatureOfWork = NewFamBack.NatureOfWork;
                        fg.Company = NewFamBack.Company;
                        fg.MaritalStatusID = NewFamBack.MaritalStatusID;
                        fg.SID = NewPersonalbg.SID;
                        sm.familybackgrounds.Add(fg);
                    }
                   
                  
                }
                sm.SaveChanges();

                string[] email = new string[] { NewPersonalbg.Email };
                var DencryptedPass = CustomDecrypt.Decrypt(NewPersonalbg.Password);
                MailResponse response = new MailResponse();
                MailerService.Mails mail = new Mails();
                MailerService.MailerService mailService = new MailerService.MailerService();

                mail.ApplicationGUID = "B02A6A55E3BD491ABE29E9F3BAC698F8";
                mail.ApplicationKey = "QjAyQTZBNTVFM0JENDkxQUJFMjlFOUYzQkFDNjk4RjgNCg==";
                mail.Content = NewPersonalbg.FirstName + "</br>" + "You've made a Successful Registration! " + "</br>" + "\n" + "Your Username is: " + NewPersonalbg.Email + "</br>" + "\nYour Password is: " + DencryptedPass + "</br>" + "Please Logon to " + "<a href='http://10.57.31.52:9090/userAuthentication/Loginuser'>Click here to Login</a>";
                // Console.Write(string.Join(System.Environment.NewLine, list)); 
                mail.From = "christianabaday@gmail.com";
                mail.To = email;
                mail.Subject = "SM Foundation Registration";


                response = mailService.Send(mail);
          
             
                return Json(new { Result = "OK",NewPersonalbg.SID});
                
            }
            catch (Exception ex)
            {
                
                  return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public virtual string UploadFiles(object obj)
        {


            var length = Request.ContentLength;
            var bytes = new byte[length];
            Request.InputStream.Read(bytes, 0, length);

            var attachname = Request.Headers["X-File-Name"];
            var fileSize = Request.Headers["X-File-Size"];
            var fileType = Request.Headers["X-File-Type"];
            
            Guid g = Guid.NewGuid();
            applicationattachment ap = new applicationattachment();
            ap.ATID = 1;
            ap.SID = Convert.ToInt32(Session["SidKo"]);
            ap.FIle = g.ToString() + attachname;
            ap.FileName = attachname;
            sm.applicationattachments.Add(ap);
            sm.SaveChanges();
            var saveToFileLoc = Server.MapPath("\\" + "UploadedFiles\\") + g.ToString()+attachname;
            // mag sasave
            var fileStream = new FileStream(saveToFileLoc, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write(bytes, 0, length);
            fileStream.Close();
            return string.Format("{0} bytes uploaded", bytes.Length);
            
        }
        [HttpPost]
        public virtual string UploadFilesReports(object obj)
        {


            var length = Request.ContentLength;
            var bytes = new byte[length];
            Request.InputStream.Read(bytes, 0, length);

            var attachname = Request.Headers["X-File-Name"];
            var fileSize = Request.Headers["X-File-Size"];
            var fileType = Request.Headers["X-File-Type"];

            Guid g = Guid.NewGuid();
            idattachment ap = new idattachment();
            ap.SID = Convert.ToInt32(Session["SidKo"]);
            ap.File = g.ToString() + attachname;
            ap.FileName = attachname;
            sm.idattachments.Add(ap);
            sm.SaveChanges();
            var saveToFileLoc = Server.MapPath("\\" + "UploadedFiles\\") + g.ToString() + attachname;
            // mag sasave
            var fileStream = new FileStream(saveToFileLoc, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write(bytes, 0, length);
            fileStream.Close();
            return string.Format("{0} bytes uploaded", bytes.Length);

        }
        [HttpPost]
        public virtual string UploadFilesITR(object obj)
        {


            var length = Request.ContentLength;
            var bytes = new byte[length];
            Request.InputStream.Read(bytes, 0, length);

            var attachname = Request.Headers["X-File-Name"];
            var fileSize = Request.Headers["X-File-Size"];
            var fileType = Request.Headers["X-File-Type"];

            Guid g = Guid.NewGuid();
            familyattachment ap = new familyattachment();
            ap.SID = Convert.ToInt32(Session["SidKo"]);
            ap.File = g.ToString() + attachname;
            ap.FileName = attachname;
            sm.familyattachments.Add(ap);
            sm.SaveChanges();
            var saveToFileLoc = Server.MapPath("\\" + "UploadedFiles\\") + g.ToString() + attachname;
            // mag sasave
            var fileStream = new FileStream(saveToFileLoc, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write(bytes, 0, length);
            fileStream.Close();
            return string.Format("{0} bytes uploaded", bytes.Length);

        }
        [HttpPost]
        public virtual string UploadFilesSketch(object obj)
        {


            var length = Request.ContentLength;
            var bytes = new byte[length];
            Request.InputStream.Read(bytes, 0, length);

            var attachname = Request.Headers["X-File-Name"];
            var fileSize = Request.Headers["X-File-Size"];
            var fileType = Request.Headers["X-File-Type"];

            Guid g = Guid.NewGuid();
            sketchattachment ap = new sketchattachment();
            ap.SID = Convert.ToInt32(Session["SidKo"]);
            ap.File = g.ToString() + attachname;
            ap.FileName = attachname;
            sm.sketchattachments.Add(ap);
            sm.SaveChanges();
            var saveToFileLoc = Server.MapPath("\\" + "UploadedFiles\\") + g.ToString() + attachname;
            // mag sasave
            var fileStream = new FileStream(saveToFileLoc, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write(bytes, 0, length);
            fileStream.Close();
            return string.Format("{0} bytes uploaded", bytes.Length);

        }
        public JsonResult CheckForDuplication(ApplicationFormModel appp)
        {
            var data = sm.personalbackgrounds.Where(p => p.Email.Equals(appp.personalBgs.Email, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            if (data != null)
            {
                return Json("Sorry, this name already exists", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
        
    }
}