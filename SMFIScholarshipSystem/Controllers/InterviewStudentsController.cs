﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class InterviewStudentsController : Controller
    {
        // GET: InterviewStudents
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Index()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetHomeVisitorCredentials)Session["HomeVisitorCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Loginuser", "UserAuthentication");
            }
            else if (myCredential != null) {
                if (myCredential.HomeVisitorPosition != "Interviewer")
                {
                    return RedirectToAction("Loginuser", "UserAuthentication");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return View();
            }
        }
        public ActionResult AddInterviewResult(List<studentinterview> a, int studid)
        {
           smscholarsEntities db =  new smscholarsEntities();
            var myCredential = (SMFIScholarshipSystem.Models.GetHomeVisitorCredentials)Session["HomeVisitorCredentials"];
            var updateStatus = db.personalbackgrounds.Where(x => x.SID == studid).FirstOrDefault();
            foreach (var item in a)
            {
                studentinterview newsi = new studentinterview();
                newsi.Questions = item.Questions;
                newsi.Grade = item.Grade;
                newsi.InID = myCredential.HomeVisitorUserId;
                newsi.Remarks = item.Remarks;
                newsi.SID = studid;
                db.studentinterviews.Add(newsi);
                db.SaveChanges();
            }
            updateStatus.Status = "Done For Interview";
            db.SaveChanges();
            return View();
        }
    }
}