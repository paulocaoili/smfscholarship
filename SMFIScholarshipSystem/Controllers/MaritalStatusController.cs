﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class MaritalStatusController : Controller
    {
        smscholarsEntities sm = new smscholarsEntities();
        // GET: MaritalStatus
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult addCivilStatus(maritalstatu st)
        {
            try
            {
                maritalstatu newMarital = new maritalstatu();
                newMarital.Name = st.Name;
                sm.maritalstatus.Add(newMarital);
                sm.SaveChanges();
                return Json(new { Result = "OK", Record = newMarital});

            }
            catch (Exception ex)
            {
                
                 return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateCivilStatus(maritalstatu st)
        {
            try
            {
                maritalstatu newMarital = sm.maritalstatus.Where(x => x.MaritalStatusID == st.MaritalStatusID).FirstOrDefault();
                newMarital.MaritalStatusID = st.MaritalStatusID;
                newMarital.Name = st.Name;
                sm.SaveChanges();
                MaritalStatusModel newMaritalModel = new MaritalStatusModel();
                newMaritalModel.MaritalStatusID = newMarital.MaritalStatusID;
                newMaritalModel.Name = newMarital.Name;

                return Json(new { Result = "OK", Record = newMaritalModel});

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteCivilStatus(maritalstatu st)
        {
            try
            {
              
                maritalstatu newMarital = sm.maritalstatus.Where(x => x.MaritalStatusID == st.MaritalStatusID).FirstOrDefault();
                if (newMarital == null)
                {
                    throw new Exception("MaritalStatusNotFound not found!");
                }
                sm.maritalstatus.Remove(newMarital);
                sm.SaveChanges();

                return Json(new { Result = "OK" });

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}