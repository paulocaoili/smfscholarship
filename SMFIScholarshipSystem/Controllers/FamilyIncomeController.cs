﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class FamilyIncomeController : Controller
    {
        // GET: FamilyIncome
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateFamIncome(familyincome f)
        {
            try
            {
                smscholarsEntities db = new smscholarsEntities();

                familyincome newFamilyIncomes = new familyincome();

                newFamilyIncomes.ID = f.ID;
                newFamilyIncomes.Income = f.Income;
                db.familyincomes.Add(newFamilyIncomes);
                db.SaveChanges();


                return Json(new { Result = "OK", Record = newFamilyIncomes });


            }
            catch (Exception ex)
            {
                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateFamIncome(familyincome f)
        {
            try
            {
                smscholarsEntities db = new smscholarsEntities();
                familyincome newFamilyIncomes = db.familyincomes.Where(x => x.ID == f.ID).FirstOrDefault();
                if (newFamilyIncomes == null)
                {
                    throw new Exception("CantUpdate");
                }
                newFamilyIncomes.ID = f.ID;
                newFamilyIncomes.Income = f.Income;

                db.SaveChanges();
                FamilyIncomeModel fm = new FamilyIncomeModel();
                fm.ID = newFamilyIncomes.ID;
                fm.Income = newFamilyIncomes.Income;

                return Json(new { Result = "OK", Record = fm });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteFamIncome(familyincome f)
        {
            try
            {
                smscholarsEntities db = new smscholarsEntities();
                familyincome newFamilyIncomes = db.familyincomes.Where(x => x.ID == f.ID).FirstOrDefault();
                if (newFamilyIncomes == null)
                {
                    throw new Exception("Family Income Not Found!");
                }

                db.familyincomes.Remove(newFamilyIncomes);

                db.SaveChanges();


                return Json(new { Result = "OK" });


            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}