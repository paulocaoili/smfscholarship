﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;
using SMFIScholarshipSystem.Models;
using System.Web.Security;
using SMFIScholarshipSystem.MailerService;

namespace SMFIScholarshipSystem.Controllers
{
    public class userAuthenticationController : Controller
    {
        // GET: userAuthentication
        public ActionResult Index()
        {
            return View();
        
        }
        public ActionResult Authenticate()
        {
            return View();

        }
        public ActionResult Loginuser()
        {
            //Session.Clear();
            //Session.Abandon();
            //Session.RemoveAll();
            //ModelState.Clear();
            return View();
        }
        [HttpPost]
        public ActionResult Login(ApplicationFormModel ap)
        {
            
            if (IsValid(ap.personalBgs.Email, ap.personalBgs.Password))
            {
                var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
                FormsAuthentication.SetAuthCookie(ap.personalBgs.Email, false);
                return Json(new { studid = myCredential.UserId,Result = "OK", url = "StudentProfile/Index" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ModelState.AddModelError("Check username and password", "Login Details are Wrong");
            }

            return Json(new { Result = "OK", url = "StudentProfile/Index", redirect = "true" }, JsonRequestBehavior.AllowGet);
        }
        private bool IsValid(string Useremail, string UserPassword)
        {

            int inactive = 2;
            bool IsValid = false;
            int active = 1;

            using (smscholarsEntities sm = new smscholarsEntities())
            {
                try
                {
                    
                    var emailCheck = sm.personalbackgrounds.FirstOrDefault(a => a.Email.Equals(Useremail));
                    var getPassword = sm.personalbackgrounds.Where(a => a.Email.Equals(Useremail)).Select(a => a.Password);
                    var materializedPass = getPassword.ToList();
                    var RealPass = materializedPass[0];
                    var decryptedPassword = CustomDecrypt.Decrypt(RealPass);
                    var acc = sm.personalbackgrounds.Where(a => a.Email.Equals(Useremail) && (decryptedPassword).Equals(UserPassword)).FirstOrDefault();



                    if (acc != null)
                    {
                       

                            IsValid = true;
                            acc.IsActive = active;


                            GetCredrentials cred = new GetCredrentials();
                            cred.Fname = acc.FirstName.ToString();
                            cred.UserId = acc.SID;
                            cred.PersonId = acc.ApplicationID;
                            Session["UserCredential"] = cred;                
                          
                            ViewBag.Message = "Login Unsuccessful";
                        
                    }
                   



                }
                catch (Exception)
                {
                    ViewBag.Message = "Login Unsuccessful";

                }

            }
            return IsValid;
        }
        public ActionResult Logout(personalbackground ap)
        {

            
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            try
            {
                int inactive = 0;
                smscholarsEntities smfi = new smscholarsEntities();
                ap.SID = myCredential.UserId;
                var newUser = smfi.personalbackgrounds.Where(a => a.SID.Equals(ap.SID)).FirstOrDefault();
                newUser.IsActive = inactive;
                smfi.SaveChanges();
                Session["UserCredential"] = null;
                myCredential = null;
                Session.Remove("UserCredential");
                ModelState.Clear();
                FormsAuthentication.SignOut();
                return RedirectToAction("Authenticate", "userAuthentication");
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult LoginInterviewer(smuser aps)
        {
            if (IsValidInterviewer(aps.Username, aps.Password))
            {
              
                FormsAuthentication.SetAuthCookie(aps.Username, false);
                return Json(new {Result = "OK", url = "InterviewDetails/Index" }, JsonRequestBehavior.AllowGet);
            }

            else
            {
                ModelState.AddModelError("Check username and password", "Login Details are Wrong");
            }

            return Json(new { Result = "ERROR" + ViewBag.Message, url = "userAuthentication/loginuser", redirect = "true" }, JsonRequestBehavior.AllowGet);
        }
        private bool IsValidInterviewer(string UseremailInterviewer, string UserPassInterviewer)
        {

            // int inactive = 2;
            bool IsValid = false;
            // int active = 1;

            using (smscholarsEntities sm = new smscholarsEntities())
            {
                try
                {
                    string userPass = CustomEnrypt.Encrypt(UserPassInterviewer);
                    var acc = sm.smusers.Where(x => x.Username == UseremailInterviewer && x.Password == userPass).FirstOrDefault();
                        if (acc != null)
                        {

                            IsValid = true;
                            if (acc.Position == "Admin")
                            {
                                GetAdminCredrentials intCredAdmin = new GetAdminCredrentials();
                                intCredAdmin.AdminFname = acc.Name;
                                intCredAdmin.AdminUserId = acc.InID;
                                intCredAdmin.AdminEmail = acc.Email;
                                intCredAdmin.Position = acc.Position;
                                Session["AdminCredentials"] = intCredAdmin;

                            }
                            else if (acc.Position == "Home Visitor" || acc.Position == "Approver" || acc.Position == "Interviewer")
                            {
                                GetHomeVisitorCredentials intCred = new GetHomeVisitorCredentials();
                                intCred.HomeVisitorUserId = acc.InID;
                                intCred.HomeVisitorName = acc.Email;
                                intCred.HomeVisitorEmail = acc.Name;
                                intCred.HomeVisitorPosition = acc.Position;
                                Session["HomeVisitorCredentials"] = intCred;
                                sm.SaveChanges();
                            }
                        }                         
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "Login Unsuccessful - " + ex.Message;

                }

            }
            return IsValid;
        }
        public ActionResult LogoutInterviewer(smuser ap)
        {

            try
            {
                var myCredential = (SMFIScholarshipSystem.Models.GetHomeVisitorCredentials)Session["HomeVisitorCredentials"];
                myCredential = null;

                Session.Remove("HomeVisitorCredentials");
                Session["HomeVisitorCredentials"] = null;
                ModelState.Clear();
                FormsAuthentication.SignOut();
                return RedirectToAction("Authenticate", "userAuthentication");
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult LogoutAdmin(personalbackground ap)
        {

            try
            {

                var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
                Session["AdminCredentials"] = null;
                Session.Remove("AdminCredentials");
                myCredential = null;
                ModelState.Clear();
                FormsAuthentication.SignOut();
                return RedirectToAction("Authenticate", "userAuthentication");
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult ForgotPass()
        {
            return View();
        }
        public ActionResult SendEmailForgotPass(string emailReset)
        {
            smscholarsEntities sm = new smscholarsEntities();
            personalbackground p = sm.personalbackgrounds.Where(x => x.Email == emailReset).FirstOrDefault();

            string[] email = new string[] { p.Email };
         
            MailResponse response = new MailResponse();
            MailerService.Mails mail = new Mails();
            MailerService.MailerService mailService = new MailerService.MailerService();

            mail.ApplicationGUID = "B02A6A55E3BD491ABE29E9F3BAC698F8";
            mail.ApplicationKey = "QjAyQTZBNTVFM0JENDkxQUJFMjlFOUYzQkFDNjk4RjgNCg==";
            mail.Content = p.FirstName + "</br>" + "You've requested us password reset, kindly proceed to the link and enter the 6  digit provided " + "</br>" + "\n" + "Your Code is: " + p.ForgotPassToken + "</br>" + "Please Click on this link to " + "http://localhost:54680/userAuthentication/ValidateAttempReset?AttempID=" + p.AttempId + "&&StudId=" + p.SID + "";
       
            mail.From = "christianabaday@gmail.com";
            mail.To = email;
            mail.Subject = "Password Reset";


            response = mailService.Send(mail);
            return Json(new { Result = "OK"});
        }
        public ActionResult ValidateAttempReset(string AttempID, int StudId)
        {

            smscholarsEntities sm = new smscholarsEntities();
           
            personalbackground p = sm.personalbackgrounds.Where(x => x.SID == StudId).FirstOrDefault();
            if (AttempID != p.AttempId)
            {
                return RedirectToAction("ForgotPass", "userAuthentication");

            }
            Session["ResetPassaid"] = AttempID;
            Session["ResetPasssid"] = StudId;
            return View();
        }
        public ActionResult ValidateKeyCode(string Passcode)
        {
            string aid = Session["ResetPassaid"].ToString();
            int sid =  Convert.ToInt16( Session["ResetPasssid"]);
            smscholarsEntities sm = new smscholarsEntities();
            personalbackground p = sm.personalbackgrounds.Where(x => x.SID == sid).FirstOrDefault();
            if (p.SID == sid && p.AttempId == aid)
            {
                if (p.ForgotPassToken == Passcode)
                {
                    return Json(new { Result = "OK", ppp = "/userAuthentication/UpdatePassword?aid=" + Session["ResetPassaid"].ToString() + "&&sid=" + Convert.ToInt16(Session["ResetPasssid"]) + "&&passcode=" + Passcode }, JsonRequestBehavior.AllowGet);
                    
                }
            }
            return Json(new { Result = "ERROR" });
        }
        public ActionResult UpdatePassword(string aid, int sid, string passcode) {
            smscholarsEntities sm = new smscholarsEntities();
            personalbackground p = sm.personalbackgrounds.Where(x => x.SID == sid).FirstOrDefault();
            if (p.AttempId == aid && passcode == p.ForgotPassToken)
            {
                Session["ResetPasssid"] = p.SID;
                return View();
            }
            return RedirectToAction("ValidateAttempReset", "userAuthentication");
        }
        private static string _numbers = "0123456789";
        Random random = new Random();
        public ActionResult actionsUpdatePassword(string newpass)
        {
            int sid = Convert.ToInt16(Session["ResetPasssid"]);
            var encryptPass = CustomEnrypt.Encrypt(newpass);
            smscholarsEntities sm = new smscholarsEntities();
            personalbackground p = sm.personalbackgrounds.Where(x => x.SID == sid).FirstOrDefault();
            Guid a = Guid.NewGuid();
            string s = random.Next(0, 1000000).ToString("D6");
            p.Password = encryptPass;
            p.AttempId = a.ToString();
            p.ForgotPassToken = s;
            sm.SaveChanges();
            return Json(new { Result = "OK", Url = "/userAuthentication/Authenticate" }, JsonRequestBehavior.AllowGet);
        }
    }
}