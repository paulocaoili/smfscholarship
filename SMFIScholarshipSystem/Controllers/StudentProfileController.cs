﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;
using System.IO;

namespace SMFIScholarshipSystem.Controllers
{
    public class StudentProfileController : Controller
    {
        smscholarsEntities smfi = new smscholarsEntities();
        // GET: StudentProfile
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Index(string id)
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            if (myCredential == null || id == "" || id == "0")
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }

        }
        /// <summary>
        /// ///////////////////Attachements
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult getMyAttachment()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];

            var result = (from stud in smfi.applicationattachments
                          where stud.SID == myCredential.UserId
                          select new AttachementModel
                          {
                              SID = stud.SID,
                              AAID = stud.AAID,
                              ATID = stud.ATID,
                              FIle = stud.FIle,
                              FileName = stud.FileName,
                          }).ToList<AttachementModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult getMyReportCard()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];

            var result = (from stud in smfi.idattachments
                          where stud.SID == myCredential.UserId
                          select new GetReportCard
                          {
                              SID = stud.SID,
                              IAID = stud.IAID,
                              File = stud.File,
                              FileName = stud.FileName,
                          }).ToList<GetReportCard>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult getMyITR()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];

            var result = (from stud in smfi.familyattachments
                          where stud.SID == myCredential.UserId
                          select new GetITR
                          {
                              SID = stud.SID,
                              FAID = stud.FAID,
                              File = stud.File,
                              FileName = stud.FileName,
                          }).ToList<GetITR>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult getMySketch()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];

            var result = (from stud in smfi.sketchattachments
                          where stud.SID == myCredential.UserId
                          select new GetSketch
                          {
                              SID = stud.SID,
                              SAID = stud.SAID,
                              File = stud.File,
                              FileName = stud.FileName,
                          }).ToList<GetSketch>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMyOwnPersonalBg()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];

            var result = (from stud in smfi.personalbackgrounds
                          where stud.SID == myCredential.UserId
                          select new PersonalBgAdminModel
                          {
                              SID = stud.SID,
                              ApplicationID = stud.ApplicationID,
                              FirstName = stud.FirstName,
                              LastName = stud.LastName,
                              MiddleName = stud.MiddleName,
                              ReligionID = stud.ReligionID,
                              Email = stud.Email,
                              DateOfBirth = stud.DateOfBirth,
                              MaritalStatusID = stud.MaritalStatusID,
                              Gender = stud.Gender,
                              PlaceOfBirth = stud.PlaceOfBirth,
                              Nationality = stud.Nationality,
                              Height = stud.Height,
                              Weight = stud.Weight,
                              AggregiateFamilyIncome = stud.AggregiateFamilyIncome,
                              DatePosted = stud.DatePosted,
                              HomeAddress = stud.HomeAddress,
                              Password = stud.Password,
                              IsActive = stud.IsActive,
                              Status = stud.Status,
                              PermanentAddress = stud.PermanentAddress,
                              ConfirmationDate = stud.ConfirmationDate,
                          }).ToList<PersonalBgAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMyOwnAcademicBg(AcademicBgAdminModel academicbg)
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            var p = smfi.academicgrades.Where(x => x.SID == academicbg.SID).FirstOrDefault();
            var result = (from acabg in smfi.academicbackgrounds
                          join perBg in smfi.personalbackgrounds
                          on acabg.SID equals perBg.SID
                          where acabg.SID == myCredential.UserId
                          select new AcademicBgAdminModel
                          {
                              ABID = acabg.ABID,
                              SID = acabg.SID,
                              HighSchoolName = acabg.HighSchoolName,
                              Section = acabg.Section,
                              CompleteAddress = acabg.CompleteAddress,
                              NameOfPrincipal = acabg.NameOfPrincipal,
                              SchoolTelno = acabg.SchoolTelno,



                          }).ToList<AcademicBgAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMyownTracks()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            var result = (from acatrack in smfi.academictracks
                          where acatrack.SID == myCredential.UserId
                          select new AcademicTracksModel
                          {
                              AcademicTrack1 = acatrack.AcademicTrack1,
                              GradeNumber = acatrack.GradeNumber,
                          }).ToList<AcademicTracksModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMyOwnContact(ContactAdminModel contact)
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            var p = smfi.contactnumbers.Where(x => x.SID == contact.SID).FirstOrDefault();
            var result = (from contat in smfi.contactnumbers
                          where contat.SID == myCredential.UserId
                          select new ContactAdminModel
                          {
                              SID = contat.SID,
                              ContactNumber1 = contat.ContactNumber1,
                              Type = contat.Type,

                          }).ToList<ContactAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMyOwnGrades(GradesAdminModel ap)
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            var p = smfi.academicgrades.Where(x => x.SID == ap.SID).FirstOrDefault();
            var result = (from acadgrade in smfi.academicgrades
                          where acadgrade.SID == myCredential.UserId
                          select new GradesAdminModel
                          {
                              SID = acadgrade.SID,
                              Grade = acadgrade.Grade,
                              Subject = acadgrade.Subject,
                              AGID = acadgrade.AGID,


                          }).ToList<GradesAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMyOwnAnnouncement()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            var result = (from announce in smfi.announcementforstudents
                          where announce.SID == myCredential.UserId
                          select new announcementforstudentModel
                          {
                              SID = announce.SID,
                              InID = announce.InID,
                              Announcement = announce.Announcement,



                          }).ToList<announcementforstudentModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMyOwnGwa()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            var result = (from acadGwa in smfi.gwaforstudents
                          where acadGwa.SID == myCredential.UserId
                          select new GWAforStudentsModel
                          {
                              SID = acadGwa.SID,
                              GWA = acadGwa.GWA,



                          }).ToList<GWAforStudentsModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMyAwards(AwardsAdminModel ap)
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            smscholarsEntities smfi = new smscholarsEntities();

            var p = smfi.academicawards.Where(x => x.SID == ap.SID).FirstOrDefault();
            var result = (from acadaward in smfi.academicawards
                          where acadaward.SID == myCredential.UserId
                          select new AwardsAdminModel
                          {
                              SID = acadaward.SID,
                              NameOfAward = acadaward.NameOfAward,
                              AALID = acadaward.AALID,
                              AAID = acadaward.AAID,

                          }).ToList<AwardsAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMyOrganization(OrganizationAdminModel ao)
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];

            var p = smfi.academicorganizationmemberships.Where(x => x.SID == ao.SID).FirstOrDefault();
            var result = (from org in smfi.academicorganizationmemberships
                          where org.SID == myCredential.UserId
                          select new OrganizationAdminModel
                          {
                              SID = org.SID,
                              Organization = org.Organization,
                              AOMID = org.AOMID,
                              PositionHeld = org.PositionHeld,

                          }).ToList<OrganizationAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMyFamilyMember(FamilyBgAdminModel fambam)
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            var p = smfi.familybackgrounds.Where(x => x.SID == fambam.SID).FirstOrDefault();
            var result = (from fam in smfi.familybackgrounds
                          where fam.SID == myCredential.UserId
                          select new FamilyBgAdminModel
                          {
                              FBID = fam.FBID,
                              MemberName = fam.MemberName,
                              FMID = fam.FMID,
                              DateOfBirth = fam.DateOfBirth,
                              EAID = fam.EAID,
                              LastSchoolAttended = fam.LastSchoolAttended,
                              NatureOfWork = fam.LastSchoolAttended,
                              Company = fam.Company,
                              MaritalStatusID = fam.MaritalStatusID,
                              SID = fam.SID,

                          }).ToList<FamilyBgAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public FilePathResult MyFileDl(string AppAttchDl)
        {
            try
            {
                string pathnames = AppDomain.CurrentDomain.BaseDirectory + "UploadedFiles\\";
                string fnames = Convert.ToString(AppAttchDl);
                return File(pathnames + fnames, "application/pdf", fnames);

            }
            catch (Exception)
            {
                return null;

            }






        }
        [HttpPost]
        public JsonResult GetAllAppAttach(AppAttachAdminModel atta)
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            smscholarsEntities smfi = new smscholarsEntities();
            var result = (from appAtt in smfi.applicationattachments

                          where appAtt.SID == myCredential.UserId
                          select new AppAttachAdminModel
                          {
                              AAID = appAtt.AAID,
                              ATID = appAtt.ATID,
                              FIle = appAtt.FIle,
                              SID = appAtt.SID,
                              FileName = appAtt.FileName,



                          }).ToList<AppAttachAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult studConfirm(PersonalBgAdminModel pbgConfirm)
        {
            try
            {
                var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
                personalbackground p = smfi.personalbackgrounds.Where(x => x.SID == myCredential.UserId).FirstOrDefault();

                p.Status = "For Processing";
                p.Gender = pbgConfirm.Gender;
                p.FirstName = pbgConfirm.FirstName;
                p.MiddleName = pbgConfirm.MiddleName;
                p.LastName = pbgConfirm.LastName;
                p.ConfirmationDate = DateTime.Now.ToString("MM-dd-yyyy");
                smfi.SaveChanges();


                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateAcademicGrade(academicgrade acagr)
        {
            try
            {
                academicgrade newacagr = smfi.academicgrades.Where(x => x.AGID == acagr.AGID).FirstOrDefault();
                newacagr.Grade = acagr.Grade;
                newacagr.Subject = acagr.Subject;
                smfi.SaveChanges();
                AcademicGradeModel newGrModel = new AcademicGradeModel();
                newGrModel.Subject = newacagr.Subject;
                newGrModel.Grade = newacagr.Grade;

                return Json(new { Result = "OK", Record = newGrModel });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateOrganization(AcademicOrganizationMembershipModel acaorg)
        {
            try
            {
                academicorganizationmembership newacaorg = smfi.academicorganizationmemberships.Where(x => x.AOMID == acaorg.AOMID).FirstOrDefault();
                newacaorg.PositionHeld = acaorg.PositionHeld;
                newacaorg.Organization = acaorg.Organization;
                smfi.SaveChanges();
                AcademicOrganizationMembershipModel newacaorgModel = new AcademicOrganizationMembershipModel();
                newacaorgModel.PositionHeld = newacaorg.PositionHeld;
                newacaorgModel.Organization = newacaorg.Organization;

                return Json(new { Result = "OK", Record = newacaorgModel });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateAward(AcademicAwardModel acaaward)
        {
            try
            {
                academicaward newacaaward = smfi.academicawards.Where(x => x.AAID == acaaward.AAID).FirstOrDefault();
                newacaaward.NameOfAward = acaaward.NameOfAward;
                newacaaward.AALID = acaaward.AALID;
                smfi.SaveChanges();
                AcademicAwardModel newacaawardModel = new AcademicAwardModel();
                newacaawardModel.NameOfAward = newacaaward.NameOfAward;
                newacaawardModel.AALID = newacaaward.AALID;

                return Json(new { Result = "OK", Record = newacaawardModel });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        //EDIT UPLOADED PIC
        [HttpPost]
        public virtual string UploadedEditPic(object obj)
        {

            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            var SIDss = smfi.applicationattachments.Where(x => x.SID == myCredential.UserId).FirstOrDefault();
            var length = Request.ContentLength;
            var bytes = new byte[length];
            Request.InputStream.Read(bytes, 0, length);

            var attachname = Request.Headers["X-File-Name"];
            var fileSize = Request.Headers["X-File-Size"];
            var fileType = Request.Headers["X-File-Type"];
            Guid g = Guid.NewGuid();


            SIDss.FIle = g.ToString() + attachname;
            SIDss.ATID = 1;
            SIDss.FileName = attachname;
            smfi.SaveChanges();
            AttachementModel am = new AttachementModel();
            am.FIle = SIDss.FIle;
            am.FileName = SIDss.FileName;
            var saveToFileLoc = Server.MapPath("\\" + "UploadedFiles\\") + g.ToString() + attachname;
            // mag sasave
            var fileStream = new FileStream(saveToFileLoc, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write(bytes, 0, length);
            fileStream.Close();
            return string.Format("{0} bytes uploaded", bytes.Length);

        }
        // EDIT UPLOADED REPORT CARD
        [HttpPost]
        public virtual string UploadedEditReportCard(object obj)
        {

            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            var SIDss = smfi.idattachments.Where(x => x.SID == myCredential.UserId).FirstOrDefault();
            var length = Request.ContentLength;
            var bytes = new byte[length];
            Request.InputStream.Read(bytes, 0, length);

            var attachname = Request.Headers["X-File-Name"];
            var fileSize = Request.Headers["X-File-Size"];
            var fileType = Request.Headers["X-File-Type"];
            Guid g = Guid.NewGuid();


            SIDss.File = g.ToString() + attachname;
            SIDss.FileName = attachname;
            smfi.SaveChanges();

            var saveToFileLoc = Server.MapPath("\\" + "UploadedFiles\\") + g.ToString() + attachname;
            // mag sasave
            var fileStream = new FileStream(saveToFileLoc, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write(bytes, 0, length);
            fileStream.Close();
            return string.Format("{0} bytes uploaded", bytes.Length);

        }
        // EDIT UPLOADED ITR
        [HttpPost]
        public virtual string UploadedEdititr(object obj)
        {

            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            var SIDss = smfi.familyattachments.Where(x => x.SID == myCredential.UserId).FirstOrDefault();
            var length = Request.ContentLength;
            var bytes = new byte[length];
            Request.InputStream.Read(bytes, 0, length);

            var attachname = Request.Headers["X-File-Name"];
            var fileSize = Request.Headers["X-File-Size"];
            var fileType = Request.Headers["X-File-Type"];
            Guid g = Guid.NewGuid();


            SIDss.File = g.ToString() + attachname;
            SIDss.FileName = attachname;
            smfi.SaveChanges();

            var saveToFileLoc = Server.MapPath("\\" + "UploadedFiles\\") + g.ToString() + attachname;
            // mag sasave
            var fileStream = new FileStream(saveToFileLoc, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write(bytes, 0, length);
            fileStream.Close();
            return string.Format("{0} bytes uploaded", bytes.Length);

        }
        // EDIT UPLOADED ITR
        [HttpPost]
        public virtual string UploadedEdithousesketch(object obj)
        {

            var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
            var SIDss = smfi.sketchattachments.Where(x => x.SID == myCredential.UserId).FirstOrDefault();
            var length = Request.ContentLength;
            var bytes = new byte[length];
            Request.InputStream.Read(bytes, 0, length);

            var attachname = Request.Headers["X-File-Name"];
            var fileSize = Request.Headers["X-File-Size"];
            var fileType = Request.Headers["X-File-Type"];
            Guid g = Guid.NewGuid();


            SIDss.File = g.ToString() + attachname;
            SIDss.FileName = attachname;
            smfi.SaveChanges();

            var saveToFileLoc = Server.MapPath("\\" + "UploadedFiles\\") + g.ToString() + attachname;
            // mag sasave
            var fileStream = new FileStream(saveToFileLoc, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write(bytes, 0, length);
            fileStream.Close();
            return string.Format("{0} bytes uploaded", bytes.Length);

        }

        //FAMILY BG
        [HttpPost]
        public ActionResult UpdateFamilyBg(familybackground a)
        {
            try
            {
                DateTime dob = Convert.ToDateTime(a.DateOfBirth);
                var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
                familybackground fb = smfi.familybackgrounds.Where(x => x.FBID == a.FBID && x.SID == myCredential.UserId).FirstOrDefault();
                fb.MemberName = a.MemberName;
                fb.FMID = a.FMID;
                fb.DateOfBirth = dob.ToString("MM-dd-yyyy");
                fb.EAID = a.EAID;
                fb.LastSchoolAttended = a.LastSchoolAttended;
                fb.NatureOfWork = a.NatureOfWork;
                fb.Company = a.Company;
                fb.MaritalStatusID = a.MaritalStatusID;
                smfi.SaveChanges();
                FamilyBgAdminModel newfb = new FamilyBgAdminModel();
                newfb.MemberName = fb.MemberName;
                newfb.FMID = fb.FMID;
                newfb.DateOfBirth = fb.DateOfBirth;
                newfb.EAID = fb.EAID;
                newfb.LastSchoolAttended = fb.LastSchoolAttended;
                newfb.NatureOfWork = fb.NatureOfWork;
                newfb.Company = fb.Company;
                newfb.MaritalStatusID = fb.MaritalStatusID;
                return Json(new { Result = "OK", Record = newfb });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult DeleteFamBg(familybackground a)
        {
            try
            {
                smscholarsEntities smfi = new smscholarsEntities();
                var myCredential = (SMFIScholarshipSystem.Models.GetCredrentials)Session["UserCredential"];
                familybackground fb = smfi.familybackgrounds.Where(x => x.FBID == a.FBID && x.SID == myCredential.UserId).FirstOrDefault();
                if (fb == null)
                {
                    throw new Exception("School Type not found!");
                }
                smfi.familybackgrounds.Remove(fb);
                smfi.SaveChanges();

                return Json(new { Result = "OK" });

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
        