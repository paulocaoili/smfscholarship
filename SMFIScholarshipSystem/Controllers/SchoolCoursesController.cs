﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class SchoolCoursesController : Controller
    {
        //
        // GET: /SchoolCourse/
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetSchoolCourse()
        {
            smscholarsEntities db = new smscholarsEntities();

            var result = (from sc in db.schoolcourses
                          select new SchoolCourseModel
                          {
                              SchoolCourseID = sc.SchoolCourseID,
                              CourseName = sc.CourseName,
                              SchoolID = sc.SchoolID,

                          }).ToList<SchoolCourseModel>();

            return Json(result, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult CreateSchoolCourse(schoolcours sc)
        {
            try
            {
                smscholarsEntities db = new smscholarsEntities();

                schoolcours newSchoolCourse = new schoolcours();

                newSchoolCourse.SchoolCourseID = sc.SchoolCourseID;
                newSchoolCourse.CourseName = sc.CourseName;
                newSchoolCourse.SchoolID = sc.SchoolID;

                db.schoolcourses.Add(newSchoolCourse);
                db.SaveChanges();


                return Json(new { Result = "OK", Record = newSchoolCourse });


            }
            catch (Exception ex)
            {
                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateSchoolCourse(schoolcours sc)
        {
            try
            {
                smscholarsEntities db = new smscholarsEntities();
                schoolcours newSchoolCourse = db.schoolcourses.Where(x => x.SchoolCourseID == sc.SchoolCourseID).FirstOrDefault();
                if (newSchoolCourse == null)
                {
                    throw new Exception("CantUpdate");
                }
                newSchoolCourse.SchoolCourseID = sc.SchoolCourseID;
                newSchoolCourse.CourseName = sc.CourseName;
                newSchoolCourse.SchoolID = sc.SchoolID;

                db.SaveChanges();

                SchoolCourseModel scm = new SchoolCourseModel();
                scm.SchoolCourseID = newSchoolCourse.SchoolCourseID;
                scm.CourseName = newSchoolCourse.CourseName;
                scm.SchoolID = newSchoolCourse.SchoolID;

                return Json(new { Result = "OK", Record = scm });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteSchoolCourse(schoolcours sc)
        {
            try
            {
                smscholarsEntities db = new smscholarsEntities();
                schoolcours newSchoolCourse = db.schoolcourses.Where(x => x.SchoolCourseID == sc.SchoolCourseID).FirstOrDefault();
                if (newSchoolCourse == null)
                {
                    throw new Exception("School Course Not Found!");
                }

                db.schoolcourses.Remove(newSchoolCourse);

                db.SaveChanges();


                return Json(new { Result = "OK" });


            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}