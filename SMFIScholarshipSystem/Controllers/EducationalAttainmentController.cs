﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;

namespace SMFIScholarshipSystem.Controllers
{
    public class EducationalAttainmentController : Controller
    {
        smscholarsEntities sm = new smscholarsEntities();
        // GET: EducationalAttainment
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddEducationalAttainment(educationalattainment ea)
        {
            try
            {
                educationalattainment newEa = new educationalattainment();
                newEa.Name = ea.Name;
                sm.educationalattainments.Add(newEa);
                sm.SaveChanges();
                return Json(new { Result = "OK", Record = newEa });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateEducationalAttainment(educationalattainment ea)
        {
            try
            {
                educationalattainment newEa = sm.educationalattainments.Where(x => x.EAID == ea.EAID).FirstOrDefault();
                newEa.EAID = ea.EAID;
                newEa.Name = ea.Name;
                sm.SaveChanges();
                EducationalAttainmentModel newEaModel = new EducationalAttainmentModel();
                newEaModel.EAID = newEa.EAID;
                newEaModel.Name = newEa.Name;

                return Json(new { Result = "OK", Record = newEaModel });

            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteEducationalAttainment(educationalattainment ea)
        {
            try
            {

                educationalattainment newEa = sm.educationalattainments.Where(x => x.EAID == ea.EAID).FirstOrDefault();
                if (newEa == null)
                {
                    throw new Exception("MaritalStatusNotFound not found!");
                }
                sm.educationalattainments.Remove(newEa);
                sm.SaveChanges();

                return Json(new { Result = "OK" });

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}