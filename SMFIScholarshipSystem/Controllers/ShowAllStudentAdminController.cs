﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMScholars.BLL;
using System.IO;
using SMFIScholarshipSystem.Models;
using System.Web.Script.Serialization;
using SMFIScholarshipSystem.MailerService;


namespace SMFIScholarshipSystem.Controllers
{
    public class ShowAllStudentAdminController : Controller
    {
        public string fname;
        public string pathname;
        smscholarsEntities smfi = new smscholarsEntities();
     
     /// <summary>
     /// //////////////////////////////////// Views
     /// </summary>
     /// <returns></returns>
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Index(string id)
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult FinalApproval()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult interviewResults()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult ViewAllStudents()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult AcceptedStudents()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }
        
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult DeclinedStudents()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }
        
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult ViewInterviewDetails()
        {
            return View();
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult QualifiedArea()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult QualifiedIncome()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Qualifiedgwa()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult ForExamination()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult ExamResults()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Authenticate", "UserAuthentication");
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public JsonResult getMyAttachment(PersonalBgAdminModel PId)
        {
          

            var result = (from stud in smfi.applicationattachments
                          where stud.SID == PId.SID
                          select new AttachementModel
                          {
                              SID = stud.SID,
                              AAID = stud.AAID,
                              ATID = stud.ATID,
                              FIle = stud.FIle,
                              FileName = stud.FileName,
                          }).ToList<AttachementModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult getMyReportCard(PersonalBgAdminModel PId)
        {
          

            var result = (from stud in smfi.idattachments
                          where stud.SID == PId.SID
                          select new GetReportCard
                          {
                              SID = stud.SID,
                              IAID = stud.IAID,
                              File = stud.File,
                              FileName = stud.FileName,
                          }).ToList<GetReportCard>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult getMyITR(PersonalBgAdminModel PId)
        {
           

            var result = (from stud in smfi.familyattachments
                          where stud.SID == PId.SID
                          select new GetITR
                          {
                              SID = stud.SID,
                              FAID = stud.FAID,
                              File = stud.File,
                              FileName = stud.FileName,
                          }).ToList<GetITR>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult getMySketch(PersonalBgAdminModel PId)
        {
           

            var result = (from stud in smfi.sketchattachments
                          where stud.SID == PId.SID
                          select new GetSketch
                          {
                              SID = stud.SID,
                              SAID = stud.SAID,
                              File = stud.File,
                              FileName = stud.FileName,
                          }).ToList<GetSketch>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetStudPass()
        {
            var result = (from studs in smfi.personalbackgrounds
                          join acadBack in smfi.academicbackgrounds
                          on studs.SID equals acadBack.SID
                          where studs.Status == "PassExam" || studs.Status == "FailExam"

                          select new PartialViewAdminnnModel
                          {
                              SID = studs.SID,
                              FirstName = studs.FirstName,
                              LastName = studs.LastName,
                              MiddleName = studs.MiddleName,
                              PlaceOfBirth = studs.PlaceOfBirth,
                              Nationality = studs.Nationality,
                              HomeAddress = studs.HomeAddress,
                              Status = studs.Status,

                          }).ToList<PartialViewAdminnnModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
       
      
        [HttpPost]
        public ActionResult updateStatusForInterview(List<int> StudID)
        {
            try
            {

                foreach (var item in StudID)
                {
                    personalbackground p = smfi.personalbackgrounds.Where(x => x.SID == item).FirstOrDefault();
                    p.SID = item;
                    p.Status = "Waiting For Interview";
                    smfi.SaveChanges();
                }

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetAllPersonalBg(PersonalBgAdminModel PId)
        {
          
            var p = smfi.academicgrades.Where(x => x.SID == PId.SID).FirstOrDefault();
            var result = (from stud in smfi.personalbackgrounds
                          join acadBack in smfi.academicbackgrounds
                          on stud.SID equals acadBack.SID
                          where acadBack.SID == p.SID && stud.SID == p.SID
                          select new PersonalBgAdminModel
                          {
                                SID = stud.SID,
                                ApplicationID = stud.ApplicationID,
                                FirstName  = stud.FirstName,
                                LastName = stud.LastName,
                                MiddleName = stud.MiddleName,
                                ReligionID = stud.ReligionID,
                                Email  = stud.Email,
                                DateOfBirth  = stud.DateOfBirth,
                                MaritalStatusID  = stud.MaritalStatusID,
                                Gender  = stud.Gender,
                                PlaceOfBirth  = stud.PlaceOfBirth,
                                Nationality = stud.Nationality,
                                Height  = stud.Height,
                                Weight  = stud.Weight,
                                AggregiateFamilyIncome = stud.AggregiateFamilyIncome,
                                DatePosted = stud.DatePosted,
                                HomeAddress = stud.HomeAddress,
                                Password  = stud.Password,
                                IsActive = stud.IsActive,
                                PermanentAddress = stud.PermanentAddress,
                                ConfirmationDate = stud.ConfirmationDate,

                          }).ToList<PersonalBgAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllContact(ContactAdminModel contact)
        {
           
            var p = smfi.contactnumbers.Where(x => x.SID == contact.SID).FirstOrDefault();
            var result = (from contat in smfi.contactnumbers
                          join perBg in smfi.personalbackgrounds
                          on contat.SID equals perBg.SID
                          where contat.SID == p.SID
                          select new ContactAdminModel
                          {
                              SID = contat.SID,
                              ContactNumber1 = contat.ContactNumber1,
                              Type = contat.Type,

                          }).ToList<ContactAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllGrades(GradesAdminModel ap)
        {
           
           

            var p = smfi.academicgrades.Where(x => x.SID == ap.SID).FirstOrDefault();
            var result = (from acadgrade in smfi.academicgrades
                          join perBg in smfi.personalbackgrounds
                          on acadgrade.SID equals perBg.SID
                          where acadgrade.SID == p.SID
                         select new GradesAdminModel
                          {
                              SID = acadgrade.SID,
                              Grade = acadgrade.Grade,
                              Subject = acadgrade.Subject,
                              

                          }).ToList<GradesAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetGwaAdmin(gwaAdminModel gam)
        {



            var p = smfi.gwaforstudents.Where(x => x.SID == gam.SID).FirstOrDefault();
            var result = (from acadgwa in smfi.gwaforstudents
                          join perBg in smfi.personalbackgrounds
                          on acadgwa.SID equals perBg.SID
                          where acadgwa.SID == p.SID
                          select new gwaAdminModel
                          {
                              SID = acadgwa.SID,
                              GWA = acadgwa.GWA,
                              ID = acadgwa.ID,
                              ABID = acadgwa.ABID,



                          }).ToList<gwaAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
 [HttpPost]
        public JsonResult GetMyownTracksAdmin(AcademicTracksModel at)
        {
            
            var result = (from acatrack in smfi.academictracks
                          where acatrack.SID == at.SID
                          select new AcademicTracksModel
                          {
                           AcademicTrack1 = acatrack.AcademicTrack1,
                           GradeNumber = acatrack.GradeNumber,
                          }).ToList<AcademicTracksModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllAwards(AwardsAdminModel ap)
        {

           

            var p = smfi.academicawards.Where(x => x.SID == ap.SID).FirstOrDefault();
            var result = (from acadaward in smfi.academicawards
                          join acadBg in smfi.academicbackgrounds
                          on acadaward.ABID equals acadBg.ABID
                          join perBg in smfi.personalbackgrounds
                          on acadaward.SID equals perBg.SID                         
                          where acadaward.SID == p.SID
                          select new AwardsAdminModel
                          {
                              SID = acadaward.SID,
                              ABID = acadBg.ABID,
                              NameOfAward = acadaward.NameOfAward,
                              AALID = acadaward.AALID,


                          }).ToList<AwardsAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllOrganization(OrganizationAdminModel ao)
        {
            
            var p = smfi.academicorganizationmemberships.Where(x => x.SID == ao.SID).FirstOrDefault();
            var result = (from org in smfi.academicorganizationmemberships
                          join perBg in smfi.personalbackgrounds
                          on org.SID equals perBg.SID
                          where org.SID == p.SID
                          select new OrganizationAdminModel
                          {
                              SID = org.SID,
                              Organization = org.Organization,
                              PositionHeld = org.PositionHeld,

                          }).ToList<OrganizationAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllFamilyMember(FamilyBgAdminModel fambam)
        {
            
            var p = smfi.familybackgrounds.Where(x => x.SID == fambam.SID).FirstOrDefault();
            var result = (from fam in smfi.familybackgrounds
                          join perBg in smfi.personalbackgrounds
                          on fam.SID equals perBg.SID
                          where fam.SID == p.SID
                          select new FamilyBgAdminModel
                          {
                            FBID = fam.FBID,
                            MemberName = fam.MemberName,
                            FMID = fam.FMID,
                            DateOfBirth= fam.DateOfBirth,
                            EAID = fam.EAID,
                            LastSchoolAttended = fam.LastSchoolAttended,
                            NatureOfWork = fam.LastSchoolAttended,
                            Company= fam.Company,
                            MaritalStatusID = fam.MaritalStatusID,
                            SID = fam.SID,

                          }).ToList<FamilyBgAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllAcademicBg(AcademicBgAdminModel academicbg)
        {
            
            var p = smfi.academicgrades.Where(x => x.SID == academicbg.SID).FirstOrDefault();
            var result = (from acabg in smfi.academicbackgrounds
                          join perBg in smfi.personalbackgrounds
                          on acabg.SID equals perBg.SID
                          where acabg.SID == p.SID
                          select new AcademicBgAdminModel
                          {
                                 ABID = acabg.ABID,
                                 SID = acabg.SID,
                                 HighSchoolName  = acabg.HighSchoolName,
                                 Section  = acabg.Section,
                                 CompleteAddress  = acabg.CompleteAddress,
                                 NameOfPrincipal = acabg.NameOfPrincipal,
                                 SchoolTelno = acabg.SchoolTelno,
                                 


                          }).ToList<AcademicBgAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllInterviewDetails(homevisitheader hed)
        {

            var p = smfi.homevisitheaders.Where(x => x.SID == hed.SID).FirstOrDefault();
            var result = (from details in smfi.homevisitdetails
                          join perBg in smfi.personalbackgrounds
                          on details.SID equals perBg.SID
                          where perBg.SID == hed.SID
                         
                          select new InterviewDetailsModel
                          {
                              InterviewEntryId = details.InterviewEntryId,
                              SID = details.SID,
                              QuestionId = details.QuestionId,
                              Status = details.Status,
                              Remarks = details.Remarks,
                          }).ToList<InterviewDetailsModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllInterviewerById(homevisitheader hed)
        {

            var p = smfi.homevisitheaders.Where(x => x.SID == hed.SID).FirstOrDefault();
            var result = (from details in smfi.homevisitdetails
                          join perBg in smfi.personalbackgrounds
                          on details.SID equals perBg.SID
                          where perBg.SID == hed.SID

                          select new InterviewDetailsModel
                          {
                            InID = details.InID,
                          }).Distinct().ToList<InterviewDetailsModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllAppAttach(AppAttachAdminModel atta)
        {
            
            var p = smfi.academicgrades.Where(x => x.SID == atta.SID).FirstOrDefault();
            var result = (from appAtt in smfi.applicationattachments
                          join perBg in smfi.personalbackgrounds
                          on appAtt.SID equals perBg.SID
                          where appAtt.SID == p.SID
                          select new AppAttachAdminModel
                          {
                                  AAID  = appAtt.AAID,
                                  ATID = appAtt.ATID,
                                  FIle = appAtt.FIle,
                                  SID  = appAtt.SID,
                                  FileName = appAtt.FileName,



                          }).ToList<AppAttachAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
     
        public FilePathResult GetFileFromDisk(string AppAttchDl)
        {
            try
            {
                string pathnames = AppDomain.CurrentDomain.BaseDirectory + "UploadedFiles\\";
                string fnames = Convert.ToString(AppAttchDl);
                return File(pathnames + fnames, "application/pdf", fnames);
          
            }
            catch (Exception)
            {
                return null;
               // throw;
            }
          
               
          
           
               
            
        }
        [HttpPost]
        public ActionResult UpdateStatusStudent(personalbackground PersonalupdateAdmin)
        {
            try
            {
                
                personalbackground newPg = smfi.personalbackgrounds.Where(x => x.SID == PersonalupdateAdmin.SID).FirstOrDefault();
                if (newPg == null)
                {
                    throw new Exception("Student not found!");
                }

                newPg.SID = PersonalupdateAdmin.SID;
                newPg.Status = PersonalupdateAdmin.Status;
                newPg.FirstName = PersonalupdateAdmin.FirstName;
                newPg.MiddleName = PersonalupdateAdmin.MiddleName;
                newPg.HomeAddress = PersonalupdateAdmin.HomeAddress;
                newPg.LastName = PersonalupdateAdmin.LastName;
                newPg.Nationality = PersonalupdateAdmin.Nationality;
                newPg.PlaceOfBirth = PersonalupdateAdmin.PlaceOfBirth;


                smfi.SaveChanges();

                PersonalBgAdminModel PerBg = new PersonalBgAdminModel();
                PerBg.SID = newPg.SID;
                PerBg.Status = newPg.Status;
                PerBg.FirstName = newPg.FirstName;
                PerBg.MiddleName = newPg.MiddleName;
                PerBg.HomeAddress = newPg.HomeAddress;
                PerBg.LastName = newPg.LastName;
                PerBg.Nationality = newPg.Nationality;
                PerBg.PlaceOfBirth = newPg.PlaceOfBirth;



                return Json(new { Result = "OK", Record = PerBg });

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult RegisterAdmin(smuser i)
        {
            try
            {
                smscholarsEntities smfi = new smscholarsEntities();
                smuser newInterviewer = new smuser();

                var encryptPass = CustomEnrypt.Encrypt(i.Password);
                newInterviewer.InID = i.InID;
                newInterviewer.Name = i.Name;
                newInterviewer.Company = i.Company;
                newInterviewer.Designation = i.Designation;
                newInterviewer.Username = i.Username;
                newInterviewer.Email = i.Email;
                newInterviewer.Password = encryptPass;
                newInterviewer.Position = "Admin";
                smfi.smusers.Add(newInterviewer);
                smfi.SaveChanges();
                return Json(new { Result = "OK", Record = newInterviewer });
            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult StudentComparison()
        {
            var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            if (myCredential == null)
            {
                return RedirectToAction("Loginuser", "UserAuthentication");
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult InsertMultiMuni(List<string> cityMun,string name)
        {
            try
            {
               
                foreach (var item in cityMun)
                {
                    qualifiedarea newArea = new qualifiedarea();
                    newArea.citymunDesc = item;
                    newArea.Name = name;
                    smfi.qualifiedareas.Add(newArea);
                }
               
                smfi.SaveChanges();
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {

                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetAllQualifiedAreas()
        {


            var result = (from quaArea in smfi.qualifiedareas
                          group quaArea.citymunDesc by quaArea.Name
                          into grp
                              select new QualifiedAreaAdminView
                              {
                                  NameOfArea = grp.Key,
                                  NoOfMunicipalities = grp.Distinct().Count()



                              }).ToList<QualifiedAreaAdminView>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllQualifiedMuni()
        {


            var result = (from quaArea in smfi.qualifiedareas                     
                          select new QualifiedAreaAdminView
                              {
                                  NameOfArea = quaArea.Name,
                                  Muni =  quaArea.citymunDesc



                              }).ToList<QualifiedAreaAdminView>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllQualifiedMuniFiltered(string a)
        {


            var result = (from quaArea in smfi.qualifiedareas
                          where quaArea.Name == a
                          select new QualifiedAreaAdminView
                          {
                              NameOfArea = quaArea.Name,
                              Muni = quaArea.citymunDesc



                          }).ToList<QualifiedAreaAdminView>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetStudentCompare()
        {

            var result = (from studs in smfi.personalbackgrounds
                          join studentGwa in smfi.gwaforstudents
                          on studs.SID equals studentGwa.SID
                          join acadbg in smfi.academicbackgrounds
                          on studs.SID equals acadbg.SID
                          where studs.Status == "Accepted"

                          select new PartialViewAdminnnModel
                          {
                              SID = studs.SID,
                              FirstName = studs.FirstName,
                              LastName = studs.LastName,
                              MiddleName = studs.MiddleName,
                              FamIncome = studs.AggregiateFamilyIncome,
                              gwaKo = studentGwa.GWA,
                              HomeAddress = acadbg.CompleteAddress,
                              municipalitySchool = acadbg.MunicipalityOfSchoolAddress,
                              
                          }).ToList<PartialViewAdminnnModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetStudentForExam()
        {

            var result = (from studs in smfi.personalbackgrounds
                          join studentGwa in smfi.gwaforstudents
                          on studs.SID equals studentGwa.SID
                          join acadbg in smfi.academicbackgrounds
                          on studs.SID equals acadbg.SID
                          where studs.Status == "For Exam"

                          select new PartialViewAdminnnModel
                          {
                              SID = studs.SID,
                              FirstName = studs.FirstName,
                              LastName = studs.LastName,
                              MiddleName = studs.MiddleName,
                              FamIncome = studs.AggregiateFamilyIncome,
                              Email = studs.Email,
                              gwaKo = studentGwa.GWA,
                              HomeAddress = acadbg.CompleteAddress,
                              examSchedStat =  studs.ExamSched,
                              municipalitySchool = acadbg.MunicipalityOfSchoolAddress,
                          }).ToList<PartialViewAdminnnModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetGradesStandard()
        {
             var result = (from NewGwa in smfi.gwas
                          
                          select new GWAViewAdminModel
                          {
                              gwaName = NewGwa.GWA1,
                          }).ToList<GWAViewAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetFamilyIncomeStandard()
        {
            var result = (from NewFamilyIncome in smfi.familyincomes

                          select new FamIncomAdminModel
                          {
                              fambamIncome = NewFamilyIncome.Income,
                          }).ToList<FamIncomAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteQualitfidArea(List<string> NameOfMuni)
        {
            try
            {
                foreach (var item in NameOfMuni)
                {
                    smfi.qualifiedareas.Where(p => p.Name == item).ToList().ForEach(p => smfi.qualifiedareas.Remove(p));
                }
                smfi.SaveChanges();
             
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                
                 return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

     
        }
        [HttpPost]
        public JsonResult GetStudent()
        {

            var result = (from stud in smfi.personalbackgrounds
                          where stud.Status == "Home Visitation"
                          select new PersonalBgAdminModel
                         
                          {
                              SID = stud.SID,
                              ApplicationID = stud.ApplicationID,
                              FirstName = stud.FirstName,
                              LastName = stud.LastName,
                              MiddleName = stud.MiddleName,
                              ReligionID = stud.ReligionID,
                              Email = stud.Email,
                              DateOfBirth = stud.DateOfBirth,
                              MaritalStatusID = stud.MaritalStatusID,
                              Gender = stud.Gender,
                              PlaceOfBirth = stud.PlaceOfBirth,
                              Nationality = stud.Nationality,
                              Height = stud.Height,
                              Weight = stud.Weight,
                              AggregiateFamilyIncome = stud.AggregiateFamilyIncome,
                              DatePosted = stud.DatePosted,
                              HomeAddress = stud.HomeAddress,
                              Password = stud.Password,
                              IsActive = stud.IsActive,


                          }).ToList<PersonalBgAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetStudentPendingInterview()
        {

            var result = (from stud in smfi.personalbackgrounds
                          where stud.Status == "Waiting For Interview"
                          select new PersonalBgAdminModel

                          {
                              SID = stud.SID,
                              ApplicationID = stud.ApplicationID,
                              FirstName = stud.FirstName,
                              LastName = stud.LastName,
                              MiddleName = stud.MiddleName,
                              ReligionID = stud.ReligionID,
                              Email = stud.Email,
                              DateOfBirth = stud.DateOfBirth,
                              MaritalStatusID = stud.MaritalStatusID,
                              Gender = stud.Gender,
                              PlaceOfBirth = stud.PlaceOfBirth,
                              Nationality = stud.Nationality,
                              Status = stud.Status,


                          }).ToList<PersonalBgAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
       
        [HttpPost]
        public JsonResult GetStudentResultInterview()
        {

            var result = (from stud in smfi.personalbackgrounds
                          where stud.Status == "Waiting For Interview" || stud.Status == "Done For Interview"
                          select new PersonalBgAdminModel

                          {
                              SID = stud.SID,
                              ApplicationID = stud.ApplicationID,
                              FirstName = stud.FirstName,
                              LastName = stud.LastName,
                              MiddleName = stud.MiddleName,
                              ReligionID = stud.ReligionID,
                              Email = stud.Email,
                              DateOfBirth = stud.DateOfBirth,
                              MaritalStatusID = stud.MaritalStatusID,
                              Gender = stud.Gender,
                              PlaceOfBirth = stud.PlaceOfBirth,
                              Nationality = stud.Nationality,
                              Status = stud.Status,


                          }).ToList<PersonalBgAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
       
        /// <summary>
        /// ////////Posiible null
        /// </summary>
        /// <returns></returns>
        
       
        [HttpPost]
        public JsonResult GetDoneApprovedStud()
        {

            var result = (from stud in smfi.personalbackgrounds
                          where stud.Status == "For Approval"
                        
                          select new PersonalBgAdminModel

                          {
                              SID = stud.SID,
                              ApplicationID = stud.ApplicationID,
                              FirstName = stud.FirstName,
                              LastName = stud.LastName,
                              MiddleName = stud.MiddleName,
                              ReligionID = stud.ReligionID,
                              Email = stud.Email,
                              DateOfBirth = stud.DateOfBirth,
                              MaritalStatusID = stud.MaritalStatusID,
                              Gender = stud.Gender,
                              PlaceOfBirth = stud.PlaceOfBirth,
                              Nationality = stud.Nationality,
                              Height = stud.Height,
                              Weight = stud.Weight,
                              AggregiateFamilyIncome = stud.AggregiateFamilyIncome,
                              DatePosted = stud.DatePosted,
                              HomeAddress = stud.HomeAddress,
                              Password = stud.Password,
                              IsActive = stud.IsActive,


                          }).ToList<PersonalBgAdminModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        ////////Posiible null
        [HttpPost]
        public ActionResult Createinterview(List<questionnaire> dt, int studId, string status, string date)
        {
            try
            {
                var InterviewerCredential = (SMFIScholarshipSystem.Models.GetHomeVisitorCredentials)Session["HomeVisitorCredentials"];
                  
                smscholarsEntities db = new smscholarsEntities();
              
                foreach (var item in dt)
                {
                    homevisitdetail newhomeDetail = new homevisitdetail();
                    newhomeDetail.InterviewId = null;
                    newhomeDetail.SID = studId;
                    newhomeDetail.InID = InterviewerCredential.HomeVisitorUserId;
                    newhomeDetail.QuestionId = item.QuestionId;
                    if (item.Status == null || item.Status == "false" || item.Status == "" || item.Status == "False")
                    {
                        item.Status = "false";
                    }
                    else if (item.Status == "True" || item.Status == "true")
                    {
                        item.Status = "true";

                    }



                    newhomeDetail.Status = item.Status;
                    newhomeDetail.Remarks = item.Remarks;
                    db.homevisitdetails.Add(newhomeDetail);

                }
                db.SaveChanges();
                personalbackground bg = db.personalbackgrounds.Where(x => x.SID == studId).FirstOrDefault();
                bg.Status = "For Approval";
                db.SaveChanges();
                homevisitheader newHeader = new homevisitheader();
                newHeader.SID = studId;
                newHeader.ApprovedBy = InterviewerCredential.HomeVisitorUserId;
                newHeader.DatePosted = date;
                newHeader.Status = status;
                smfi.homevisitheaders.Add(newHeader);
                smfi.SaveChanges();
                List<homevisitdetail> updateList = new List<homevisitdetail>();
                foreach (var item in dt)
                {
                    var newDetail = db.homevisitdetails.Where(x => x.SID == studId && x.QuestionId == item.QuestionId).FirstOrDefault();
                    newDetail.InterviewId = newHeader.InterviewId;
                    db.SaveChanges();

                }
                return Json(new { Result = "OK"});


            }
            catch (Exception ex)
            {
                return Json(new { result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetStudentExamResult()
        {
            MoodleLinkCL mlc = new MoodleLinkCL();

            var getUser = smfi.moodleintegrations.ToList();
            List<userGrades> ugs = new List<userGrades>();
            foreach (var user in getUser)
            {
                mlc.moodleid = user.MoodleID;
                mlc.getStudentExamResults();
                if (mlc.errorMessage == "")
                {
                    activityItems result = mlc.gradeItems;

                    // foreach (var grades in mlc.gradeItems.items.First())
                    // {
                    var grades = mlc.gradeItems.items.First();
                    foreach (var grade in grades.grades)
                    {
                        userGrades ug = new userGrades();
                        ug = grade;
                        ugs.Add(ug);
                        var updatemoddleintegrate = smfi.moodleintegrations.Where(x => x.MoodleID == ug.userid).FirstOrDefault();
                        updatemoddleintegrate.Grade =  ug.str_grade;
                        smfi.SaveChanges();
                        
                    }
                    // }
                }
                else
                {

                    string error = mlc.errorMessage;
                }

            }
            List<PartialViewAdminnnModel> results = (from a in ugs
                                                     join b in smfi.moodleintegrations on a.userid equals b.MoodleID
                                                     join c in smfi.personalbackgrounds on b.SID equals c.SID
                                                     select new PartialViewAdminnnModel
                                                        {
                                                            SID = c.SID,
                                                            FirstName = c.FirstName,
                                                            LastName = c.LastName,
                                                            MiddleName = c.MiddleName,
                                                            FamIncome = c.AggregiateFamilyIncome,
                                                            grade = a.str_grade,
                                                            HomeAddress = "",
                                                            Status = c.Status,

                                                        }).ToList<PartialViewAdminnnModel>();
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetStudentExamResultPercentage(personalbackground pi)
        {
            var result = (from moodleGrade in smfi.moodleintegrations
                          where moodleGrade.SID == pi.SID
                          select new MoodleGradesModel
                          {
                              SID = moodleGrade.SID,
                              ModdleGrade = moodleGrade.Grade,

                          }).ToList<MoodleGradesModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
/// ////////////////////////////////// STEPS
/// </summary>
/// <returns></returns>
        //STEP 1 ////FOR PROCCESSING
        [HttpPost]
        public JsonResult PartialDetails()
        {
            var result = (from studs in smfi.personalbackgrounds
                          join acadBack in smfi.academicbackgrounds
                          on studs.SID equals acadBack.SID
                          where studs.Status == "For Processing"

                          select new PartialViewAdminnnModel
                          {
                              SID = studs.SID,
                              FirstName = studs.FirstName,
                              LastName = studs.LastName,
                              MiddleName = studs.MiddleName,
                              PlaceOfBirth = studs.PlaceOfBirth,
                              Nationality = studs.Nationality,
                              HomeAddress = studs.HomeAddress,
                              Status = studs.Status,

                          }).ToList<PartialViewAdminnnModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //STEP 2 //// ACCEPTED/DECLINED
        [HttpPost]
        public JsonResult ViewAllStudent()
        {

            var result = (from studs in smfi.personalbackgrounds
                          join acadBack in smfi.academicbackgrounds
                          on studs.SID equals acadBack.SID
                        


                          select new PartialViewAdminnnModel
                          {
                              SID = studs.SID,
                              FirstName = studs.FirstName,
                              LastName = studs.LastName,
                              MiddleName = studs.MiddleName,
                              PlaceOfBirth = studs.PlaceOfBirth,
                              Nationality = studs.Nationality,
                              HomeAddress = studs.HomeAddress,
                              Status = studs.Status,
                              




                          }).ToList<PartialViewAdminnnModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult PartialDetailsAccepted()
        {

            var result = (from studs in smfi.personalbackgrounds
                          join acadBack in smfi.academicbackgrounds
                          on studs.SID equals acadBack.SID
                          where studs.Status == "Accepted"


                          select new PartialViewAdminnnModel
                          {
                              SID = studs.SID,
                              FirstName = studs.FirstName,
                              LastName = studs.LastName,
                              MiddleName = studs.MiddleName,
                              PlaceOfBirth = studs.PlaceOfBirth,
                              Nationality = studs.Nationality,
                              HomeAddress = studs.HomeAddress,
                              Status = studs.Status,




                          }).ToList<PartialViewAdminnnModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult PartialDetailsDeclined()
        {

            var result = (from studs in smfi.personalbackgrounds
                          join acadBack in smfi.academicbackgrounds
                          on studs.SID equals acadBack.SID
                          where studs.Status == "Declined"


                          select new PartialViewAdminnnModel
                          {
                              SID = studs.SID,
                              FirstName = studs.FirstName,
                              LastName = studs.LastName,
                              MiddleName = studs.MiddleName,
                              PlaceOfBirth = studs.PlaceOfBirth,
                              Nationality = studs.Nationality,
                              HomeAddress = studs.HomeAddress,
                              Status = studs.Status,




                          }).ToList<PartialViewAdminnnModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        ///STEP 3 /// FOR EXAM
        [HttpPost]
        public ActionResult updatePartialAcceptance(List<int> StudID)
        {
            try
            {

                foreach (var item in StudID)
                {
                    moodleintegration mi = new moodleintegration();
                    personalbackground p = smfi.personalbackgrounds.Where(x => x.SID == item).FirstOrDefault();
                    p.SID = item;
                    p.Status = "For Exam";
                    smfi.SaveChanges();
                    MoodleUser mu = new MoodleUser();
                    mu.email = p.Email;
                    mu.firstname = p.FirstName;
                    mu.lastname = p.LastName;
                    var decPass = CustomDecrypt.Decrypt(p.Password);
                    mu.password = decPass;//decPass;
                    mu.username = p.Email;

                    MoodleLinkCL mll = new MoodleLinkCL();
                    mll.createModelUser(mu);
                    mi.SID = p.SID;
                    mi.MoodleID = Convert.ToInt16(mll.moodleid);
                    smfi.moodleintegrations.Add(mi);
                    smfi.SaveChanges();
                }
               
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult AddExamSchdeulDetails(List<int> StudId,List<string>StudEmail, string ProctorEmail,string nameOfProctor,string VenueOfExam,string Time,string dateExam)
        {
               var myCredential = (SMFIScholarshipSystem.Models.GetAdminCredrentials)Session["AdminCredentials"];
            foreach (var item1 in StudId)
            {
                examscheduledetail examdet = new examscheduledetail();
                var p = smfi.personalbackgrounds.Where(x => x.SID == item1).FirstOrDefault();
                p.ExamSched = true;
                examdet.SID = item1;
                examdet.NameOfProctor = nameOfProctor;
                examdet.EmailProctor = ProctorEmail;
                examdet.Venue = VenueOfExam;
                examdet.Time = Time;
                examdet.InID = myCredential.AdminUserId;
                examdet.Date = dateExam;
                smfi.examscheduledetails.Add(examdet);
                smfi.SaveChanges();
                announcementforstudent newAnnouncement = new announcementforstudent();
                newAnnouncement.InID = myCredential.AdminUserId;
                newAnnouncement.SID = item1;
                newAnnouncement.Announcement = "Your Exam Schedule Details Has been sent to your Email";
                newAnnouncement.Date = DateTime.Now.ToString("mm-dd-yyyy");
                smfi.announcementforstudents.Add(newAnnouncement);
                smfi.SaveChanges();
            }
            string[] email =  StudEmail.ToArray();
            MailResponse response = new MailResponse();
            MailerService.Mails mail = new Mails();
            MailerService.MailerService mailService = new MailerService.MailerService();

            mail.ApplicationGUID = System.Configuration.ConfigurationManager.AppSettings["ApplicationGUID"];
            mail.ApplicationKey = System.Configuration.ConfigurationManager.AppSettings["ApplicationKey"];
            mail.Content = "Exam Schedule" + "</br>"
                            + "\n" + "Date of Exam" + Convert.ToDateTime(dateExam).ToString("MMMM dd yyyy") +"</br>"
                            + "\nTime Of Exam: " + Time + "</br>"
                            + "\nVenue of Exam: " + VenueOfExam + "</br>"
                            + "\nYour Proctor Name: " + nameOfProctor + "</br>";
            mail.From = "christianabaday@gmail.com";
            mail.To = email;
            mail.Subject = "Your Exam Schedule";
            response = mailService.Send(mail);

            string[] emailAddressProctor = new string[] { ProctorEmail };
            MailResponse responseProctor = new MailResponse();
            MailerService.Mails mailProctor = new Mails();
            MailerService.MailerService mailServiceProctor = new MailerService.MailerService();
            mailProctor.ApplicationGUID = System.Configuration.ConfigurationManager.AppSettings["ApplicationGUID"];
            mailProctor.ApplicationKey = System.Configuration.ConfigurationManager.AppSettings["ApplicationKey"];
            mailProctor.Content = "Exam Schedule" + "</br>"
                            + "\n" + "Date of Exam" + Convert.ToDateTime(dateExam).ToString("MMMM dd yyyy") + "</br>"
                            + "\nTime Of Exam: " + Time + "</br>"
                            + "\nVenue of Exam: " + VenueOfExam + "</br>"
                            + "\nNumber of Students you'll be handling: " + StudId.Count + "Students"+ "</br>";
            mailProctor.From = "christianabaday@gmail.com";
            mailProctor.To = emailAddressProctor;
            mailProctor.Subject = "Good Day! Your Exam Schedule";
            responseProctor = mailServiceProctor.Send(mailProctor);
          return Json(new { Result = "OK",Response=response, Responses =responseProctor });
            
        }
        ////STEP 4// Waiting For Interview
        [HttpPost]
        public ActionResult updateWaitingForinterview(List<int> SID)
        {
            try
            {

                foreach (var item in SID)
                {
                    personalbackground p = smfi.personalbackgrounds.Where(x => x.SID == item).FirstOrDefault();
                    p.SID = item;
                    p.Status = "Waiting for Interview";
                    smfi.SaveChanges();
                }

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        ///// STEP 5// For Home Visit
        [HttpPost]
        public ActionResult updateForHomeVisit(int pi, int visibtorID)
        {
            try
            {
                studenthomevisit st = new studenthomevisit();
                var y = smfi.personalbackgrounds.Where(x => x.SID == pi).FirstOrDefault();
                y.Status = "Home Visitation";
                //smfi.SaveChanges();
                
                st.SID = pi;
                st.InID = visibtorID;
                st.StudentAddress = y.HomeAddress;
                smfi.studenthomevisits.Add(st);
                smfi.SaveChanges();
                return Json(new { Result = "Student has been assign to home visitor" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }                                     
    ///Final Approval View
        [HttpPost]
        public JsonResult FinalApprovaLView()
        {
            var result = (from pbg in smfi.personalbackgrounds 
                          join acbg in smfi.academicbackgrounds
                          on pbg.SID equals acbg.SID
                          join gwass in smfi.gwaforstudents
                          on acbg.SID equals gwass.SID
                          join moo in smfi.moodleintegrations
                          on gwass.SID equals moo.SID
                          join home in smfi.homevisitheaders
                          on moo.SID equals home.SID
                          join interviewgrae in smfi.studentinterviews
                          on moo.SID equals interviewgrae.SID into g
                          where moo.SID == pbg.SID
                          select new FinalApprovalViewModel
                          {
                              SID = pbg.SID,
                              FirstName = pbg.FirstName,
                              MiddleName = pbg.MiddleName,
                              LastName = pbg.LastName,
                              AggregiateFamilyIncome = pbg.AggregiateFamilyIncome,
                              CompleteAddress = acbg.CompleteAddress,
                              GWA = gwass.GWA,
                              StatusInterviewResult = home.Status,
                              interviewGrade = g.Sum(x => x.Grade),
                              Grade = moo.Grade,


                          }).ToList<FinalApprovalViewModel>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}