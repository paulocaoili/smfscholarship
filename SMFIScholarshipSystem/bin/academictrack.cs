//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMScholars.BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class academictrack
    {
        public int AcademicTrackID { get; set; }
        public int ABID { get; set; }
        public int SID { get; set; }
        public string GradeNumber { get; set; }
        public string AcademicTrack1 { get; set; }
    
        public virtual academicbackground academicbackground { get; set; }
        public virtual personalbackground personalbackground { get; set; }
    }
}
