//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMScholars.BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class studentinterview
    {
        public int StudentInterviewID { get; set; }
        public int SID { get; set; }
        public string Questions { get; set; }
        public int Grade { get; set; }
        public int InID { get; set; }
        public string Remarks { get; set; }
    
        public virtual personalbackground personalbackground { get; set; }
        public virtual smuser smuser { get; set; }
    }
}
