//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMScholars.BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class familybackground
    {
        public int FBID { get; set; }
        public string MemberName { get; set; }
        public int FMID { get; set; }
        public string DateOfBirth { get; set; }
        public int EAID { get; set; }
        public string LastSchoolAttended { get; set; }
        public string NatureOfWork { get; set; }
        public string Company { get; set; }
        public int MaritalStatusID { get; set; }
        public int SID { get; set; }
    
        public virtual educationalattainment educationalattainment { get; set; }
        public virtual familymember familymember { get; set; }
        public virtual maritalstatu maritalstatu { get; set; }
        public virtual personalbackground personalbackground { get; set; }
    }
}
