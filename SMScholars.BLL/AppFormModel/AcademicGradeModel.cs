﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class AcademicGradeModel
    {
        public int AGID { get; set; }
        public string Subject { get; set; }
        public string Grade { get; set; }
        public int ABID { get; set; }
        public int SID { get; set; }
    }
}
