﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class AcademicOrganizationMembershipModel
    {
        public int AOMID { get; set; }
        public string Organization { get; set; }
        public string PositionHeld { get; set; }
        public int SID { get; set; }
        public int ABID { get; set; }
    }
}
