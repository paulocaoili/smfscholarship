﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class ApplicationFormModel
    {
       
        public PersonalBgcust personalBgs { get; set; }
        public AcademicBackgroundModel Academicbackground { get; set; }
        public List<AcademicGradeModel> AcademicGrade { get; set; }
        public List<contactnumber> contactInfo { get; set; }
        public List<academicawardlevel> academiclvlAwards { get; set; }
        public List<academicorganizationmembership> acadorg { get; set; }
        public List<academicaward> acadAwards { get; set; }
        public List<collegeplan> colpnas { get; set; }
        public List<familybackground> famBack { get; set; }
        public List<applicationattachment> appattachmenttypes { get; set; }
        public StudPassword pass { get; set; }
        public smuser intLog
        {
            get;
            set;
        }
        public string municitySchool { get;set;}
        public List<qualifiedarea> areas { get; set; }
        public gwaforstudent gwaofStud { get; set; }
        public List<academictrack> acadtrack { get; set; }
  
      

    }

}
