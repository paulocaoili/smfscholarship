﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
   public class AcademicBackgroundModel
    {
        public int ABID { get; set; }
        public int SID { get; set; }
        public string HighSchoolName { get; set; }
        public string Section { get; set; }
        public string CompleteAddress { get; set; }
        public string NameOfPrincipal { get; set; }
        public string SchoolTelno { get; set; }
        
    }
}
