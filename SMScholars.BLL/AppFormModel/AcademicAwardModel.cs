﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class AcademicAwardModel
    {
        public int AALID { get; set; }
        public string NameOfAward { get; set; }
        public int AAID { get; set; }
    }
}
