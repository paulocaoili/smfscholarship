﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class AwardsAdminModel
    {
        public int AAID { get; set; }
        public string NameOfAward { get; set; }
        public int AALID { get; set; }
        public int SID { get; set; }
        public int ABID { get; set; }
    }
}
