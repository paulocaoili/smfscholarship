﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class ContactAdminModel
    {
        public int CNID { get; set; }
        public int SID { get; set; }
        public string Type { get; set; }
        public string ContactNumber1 { get; set; }
    }
}
