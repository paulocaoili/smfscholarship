﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
     public class announcementforstudentModel
    {
        public int PID { get; set; }
        public int SID { get; set; }
        public int InID { get; set; }
        public string Announcement { get; set; }
    }
}
