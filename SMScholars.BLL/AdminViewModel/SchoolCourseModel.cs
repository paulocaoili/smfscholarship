﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class SchoolCourseModel
    {
        public int SchoolCourseID { get; set; }
        public string CourseName { get; set; }
        public int SchoolID { get; set; }
        public int SchoolName { get; set; }
    }
}
