﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class QualifiedAreaAdminView
    {
        public string NameOfArea { get; set; }
        public int NoOfMunicipalities { get; set; }
        public string Muni { get; set; }
        public string Name { get; set; }
        
    }
}
