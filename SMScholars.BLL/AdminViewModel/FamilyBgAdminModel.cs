﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class FamilyBgAdminModel
    {
        public int FBID { get; set; }
        public string MemberName { get; set; }
        public int FMID { get; set; }
        public string DateOfBirth { get; set; }
        public int EAID { get; set; }
        public string LastSchoolAttended { get; set; }
        public string NatureOfWork { get; set; }
        public string Company { get; set; }
        public int MaritalStatusID { get; set; }
        public int SID { get; set; }
        public int editfbg { get; set; }
    }
}
