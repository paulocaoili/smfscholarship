﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class AcademicTracksModel
    {
        public int AcademicTrackID { get; set; }
        public int ABID { get; set; }
        public int SID { get; set; }
        public string GradeNumber { get; set; }
        public string AcademicTrack1 { get; set; }
    }
}
