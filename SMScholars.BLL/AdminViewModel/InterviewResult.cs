﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class InterviewResult
    {
        public int StudentInterviewID { get; set; }
        public int SID { get; set; }
        public string Questions { get; set; }
        public int Grade { get; set; }
        public int InID { get; set; }
        public string Remarks { get; set; }
    }
}
