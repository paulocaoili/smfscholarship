﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class OrganizationAdminModel
    {
        public int AOMID { get; set; }
        public string Organization { get; set; }
        public string PositionHeld { get; set; }
        public int SID { get; set; }
        public int ABID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
