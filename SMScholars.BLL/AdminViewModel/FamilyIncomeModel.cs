﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class FamilyIncomeModel
    {
        public int ID { get; set; }
        public Nullable<float> Income { get; set; }
    }
}
