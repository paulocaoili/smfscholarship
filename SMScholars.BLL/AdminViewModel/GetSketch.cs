﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class GetSketch
    {
        public int SAID { get; set; }
        public string File { get; set; }
        public int SID { get; set; }
        public string FileName { get; set; }
    }
}
