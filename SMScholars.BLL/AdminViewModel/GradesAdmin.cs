﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class GradesAdminModel
    {
        public int SID { get; set; }
        public string Subject { get; set; }
        public int AGID { get; set; }
        public string Grade { get; set; }
        public int AOMID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string editsubjgrade { get; set; }
    }
}
