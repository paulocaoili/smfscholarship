﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;


namespace SMScholars.BLL
{
    public class PersonalBgAdminModel
    {
        public int SID { get; set; }
        public string ApplicationID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int ReligionID { get; set; }
        public string Email { get; set; }
        public string DateOfBirth { get; set; }
        public int MaritalStatusID { get; set; }
        public string Gender { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Nationality { get; set; }
        public string Height { get; set; }
        public string ConfirmationDate { get; set; }
        public string Weight { get; set; }
        public string AggregiateFamilyIncome { get; set; }
        public string DatePosted { get; set; }
        public string HomeAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string Password { get; set; }
        public string Status { get; set; }
        public int IsActive { get; set; }
       
       
     
    }
}
