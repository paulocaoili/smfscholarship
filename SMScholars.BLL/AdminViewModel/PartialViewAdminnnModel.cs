﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class PartialViewAdminnnModel
    {
        public int SID { get; set; }
        public string FirstName { get; set; }
        public string Interviewer { get; set; }
        public string municipalitySchool { get; set; }
        public string ModdleGrade { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Gender { get; set; }
        public string Nationality { get; set; }
        public string HomeAddress { get; set; }
        public Nullable<bool> examSchedStat { get; set; }
        public string Status { get; set; }
        public string FamIncome { get; set; }
        public int gwaKo { get; set; }
        public string grade { get; set; }
        public Boolean IsQualifiedGWA{
        get{
    
      smscholarsEntities db = new smscholarsEntities();

      var result = db.gwas.Where(a => a.GWA1 <= this.gwaKo).FirstOrDefault();
            if (result == null)
            {
                return false;
            }

            return true;
            }
    
        }
   
        public Boolean IsQualified { get {

            smscholarsEntities db = new smscholarsEntities();

            var result = db.qualifiedareas.Where(x => x.citymunDesc == this.munionly).FirstOrDefault();
            if (result == null)
            {
                return false;
            }

            return true;
        
        } }
        public string munionly { get {

            var places = this.HomeAddress.Split(',');

            if (places.Length == 3)
            {

                return places[1].ToString();
            }
           
            else
            {
                return "";
            }
        
        
        } }
    }
}
