﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
     public class MunicipalityList
    {
         public int id {get;set;}
         public string psgcCode {get;set;}
         public string citymunDesc {get;set;}
         public string regDesc {get;set;}
         public string provCode {get;set;}
         public string citymunCode {get;set;}
       
    }
}
