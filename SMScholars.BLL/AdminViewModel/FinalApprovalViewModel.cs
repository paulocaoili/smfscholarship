﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class FinalApprovalViewModel
    {
        public int SID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string AggregiateFamilyIncome { get; set; }
        public string CompleteAddress { get; set; }
        public int GWA { get; set; }
        public string Grade { get; set; }
        public string StatusInterviewResult { get; set; }
        public int interviewGrade { get; set; }
    
    }
}
