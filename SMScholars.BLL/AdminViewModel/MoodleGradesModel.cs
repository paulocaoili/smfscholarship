﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
   public class MoodleGradesModel
    {
        public int SID { get; set; }
        public string FirstName { get; set; }
        public string Interviewer { get; set; }
        public string ModdleGrade { get; set; }
    }
}
