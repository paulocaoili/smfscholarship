﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class InterviewDetailsModel
    {
        public int InterviewEntryId { get; set; }
        public Nullable<int>InterviewId { get; set; }
        public int InID { get; set; }
        public int SID { get; set; }
        public int QuestionId { get; set; }
        public string Remarks { get; set; }
        public string StudFname { get; set; }
        public string StudLname { get; set; }
        public string StudMname { get; set; }
        public string HomeAddress { get; set; }
        public string DatePostedByAdmin { get; set; }
        public string NoOfPoints { get; set; }
        public string Status { get; set; }
        public string ApprovedBy{ get; set; }
        public string StatusByAdmin { get; set; }
    }
}
