﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class FamilyMemberModel
    {
        public int FMID { get; set; }
        public string Name { get; set; }
    }
}
