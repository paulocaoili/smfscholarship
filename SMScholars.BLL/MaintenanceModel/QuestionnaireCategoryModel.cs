﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class QuestionnaireCategoryModel
    {
        public int CategoryId { get; set; }
        public string Description { get; set; }

    }
}
