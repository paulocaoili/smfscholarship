﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
   public class QuestionnaireModel
    {
       public int QuestionId { get; set; }
        public string Question { get; set; }
        public string Year { get; set; }
        public int CategoryId { get; set; }

    }
}
