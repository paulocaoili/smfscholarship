﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class MaritalStatusModel
    {
        public int MaritalStatusID { get; set; }
        public string Name { get; set; }
    }
}
