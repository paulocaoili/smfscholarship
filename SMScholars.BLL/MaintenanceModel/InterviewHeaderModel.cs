﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
    public class InterviewHeaderModel
    {
        public int InterviewId { get; set; }
        public int InterviewerId { get; set; }
        public int SID { get; set; }
        public string DatePosted { get; set; }
        public string Remarks { get; set; }
        public string NoOfPoints { get; set; }
        public string Status { get; set; }
        public int ApprovedBy { get; set; }

    }
}
