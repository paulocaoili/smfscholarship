﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
   public class EducationalAttainmentModel
    {
        public int EAID { get; set; }
        public string Name { get; set; }
    }
}
