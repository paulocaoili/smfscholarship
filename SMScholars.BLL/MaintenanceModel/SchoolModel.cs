﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMScholars.BLL
{
   public class SchoolModel
    {
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
    }
}
