using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Text;
using System.Collections.Generic;

public class MoodleLinkCL
{
    public string createModelUser()
    {

        string token = "85db2e21c1cd36a0968c69a7cb674aab";
            MoodleUser user = new MoodleUser();
            user.username = HttpUtility.UrlEncode("nico.bulusan@sminvestmentss.com");
            user.password = HttpUtility.UrlEncode("P@ssw0rd");
            user.firstname = HttpUtility.UrlEncode("Nico");
            user.lastname = HttpUtility.UrlEncode("Bulusan");
            user.email = HttpUtility.UrlEncode("nico.bulusans@sminvestments.com");

            List<MoodleUser> userList = new List<MoodleUser>();
            userList.Add(user);

            Array arrUsers = userList.ToArray();
          String postData = String.Format("users[0][username]={0}&users[0][password]={1}&users[0][firstname]={2}&users[0][lastname]={3}&users[0][email]={4}", user.username, user.password, user.firstname, user.lastname, user.email);



            string createRequest = string.Format("http://10.57.30.132/moodle/webservice/rest/server.php?wstoken={0}&wsfunction={1}&moodlewsrestformat=json", token, "core_user_create_users");
        /*
            String postData = String.Format("users[0][username]={0}&users[0][password]={1}&users[0][firstname]={2}&users[0][lastname]={3}&users[0][email]={4}", user.username, user.password, user.firstname, user.lastname, user.email);



            string createRequest = string.Format("http://10.57.30.132/moodle/webservice/rest/server.php?wstoken={0}&wsfunction={1}&moodlewsrestformat=json", token, "core_user_create_users");
         
        

            String postData = String.Format("[courseid]={0}&[userid]={1}", 2, 6);



            string createRequest = string.Format("http://10.57.30.132/moodle/webservice/rest/server.php?wstoken={0}&wsfunction={1}&moodlewsrestformat=json", token, "GetGrades");
            */
         // Call Moodle REST Service

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(createRequest);

            req.Method = "POST";

            req.ContentType = "application/x-www-form-urlencoded";

              // Encode the parameters as form data:

            byte[] formData =
                UTF8Encoding.UTF8.GetBytes(postData);
            req.ContentLength = formData.Length;

            // Write out the form Data to the request:
            using (Stream post = req.GetRequestStream())
            {
                post.Write(formData, 0, formData.Length);
            }


            // Get the Response
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            Stream resStream = resp.GetResponseStream();
            StreamReader reader = new StreamReader(resStream);
            string contents = reader.ReadToEnd();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if (contents.Contains("exception"))
            {
                // Error
                MoodleException moodleError = serializer.Deserialize<MoodleException>(contents);
            }
            else
            {
                // Good
                List<MoodleCreateUserResponse> newUsers = serializer.Deserialize<List<MoodleCreateUserResponse>>(contents);
            }
        
            return "";

    }
    
        public class MoodleUser

        {

            public string username { get; set; }

            public string password { get; set; }

            public string firstname { get; set; }

            public string lastname { get; set; }

            public string email { get; set; }

        }


        public class MoodleEnrollment

        {

            public int roleId { get; set; }

            public int userId { get; set; }

            public int courseId { get; set; }

        }

        public class MoodleException

        {

            public string exception { get; set; }

            public string errorcode { get; set; }

            public string message { get; set; }

            public string debuginfo { get; set; }

        }


        public class MoodleCreateUserResponse

        {

            public string id { get; set; }

            public string username { get; set; }

        }

    
}